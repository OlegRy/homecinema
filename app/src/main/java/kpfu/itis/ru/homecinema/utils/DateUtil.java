package kpfu.itis.ru.homecinema.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

    private static final String TIME_FORMAT = "HH:mm";

    public static String formatDate(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat(TIME_FORMAT);
        return sdf.format(date);
    }
}
