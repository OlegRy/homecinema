package kpfu.itis.ru.homecinema.ui.fragments;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import kpfu.itis.ru.homecinema.R;
import kpfu.itis.ru.homecinema.models.Movie;
import kpfu.itis.ru.homecinema.services.MoviesService;
import kpfu.itis.ru.homecinema.ui.MoviesActivity;
import kpfu.itis.ru.homecinema.ui.adapters.ShowingMovieAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class MovieListFragment extends Fragment implements ShowingMovieAdapter.OnItemClickListener {

    public static final String GET_MOVIES_FILTER = "MovieListFragment.filter.GET_MOVIES";
    public static final String EXTRA_MOVIES = "MovieListFragment.extra.MOVIES";

    private static final String BUNDLE_GENRE = "kpfu.itis.ru.homecinema.ui.fragments.ShowingMoviesListFragment.bundle.GENRE";

    private FragmentViewHolder mFragmentViewHolder;
    private OnMovieClickListener mCallback;
    private MaterialDialog mDialog;
    private ShowingMovieAdapter mAdapter;

    private String mCurrentGenre;

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            List<Movie> movies = (ArrayList<Movie>) intent.getSerializableExtra(EXTRA_MOVIES);
            if (containsGenre(movies)) initAdapter(movies);
        }
    };

    private boolean containsGenre(List<Movie> movies) {
        for (Movie movie : movies) {
            if (!movie.getGenre().equalsIgnoreCase(mCurrentGenre)) return false;
        }
        return true;
    }

    public static MovieListFragment newInstance(String genre) {
        MovieListFragment fragment = new MovieListFragment();
        Bundle args = new Bundle();
        args.putString(BUNDLE_GENRE, genre);
        fragment.setArguments(args);
        return fragment;
    }

    public interface OnMovieClickListener {
        void onMovieClick(Movie movie);
    }

    class FragmentViewHolder {
        @Bind(R.id.rv_movies) RecyclerView mMoviesRecyclerView;
    }

    public MovieListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) mCurrentGenre = args.getString(BUNDLE_GENRE);
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mReceiver, new IntentFilter(GET_MOVIES_FILTER));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movie_list, container, false);
        mFragmentViewHolder = new FragmentViewHolder();
        ButterKnife.bind(mFragmentViewHolder, view);
        showDialog();
        MoviesService.startActionGetMovies(getContext(), mCurrentGenre);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MoviesActivity) mCallback = (OnMovieClickListener) context;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mReceiver);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    private void initAdapter(List<Movie> movies) {
        GridLayoutManager manager = new GridLayoutManager(getContext(), 2);
        mAdapter = new ShowingMovieAdapter(getContext(), movies);
        mAdapter.setCallback(this);
        mFragmentViewHolder.mMoviesRecyclerView.setLayoutManager(manager);
        mFragmentViewHolder.mMoviesRecyclerView.setAdapter(mAdapter);
        hideDialog();
    }

    private void hideDialog() {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
            mDialog = null;
        }
    }

    private void showDialog() {
        populateDialog();
        mDialog.show();
    }

    private void populateDialog() {
        if (mDialog == null) {
            mDialog = new MaterialDialog.Builder(getContext())
                    .cancelable(false)
                    .title(R.string.loading)
                    .content(R.string.loading_content)
                    .progress(true, 0)
                    .build();
        }
    }

    @Override
    public void onItemClick(Movie movie) {
        if (mCallback != null) mCallback.onMovieClick(movie);
    }

}
