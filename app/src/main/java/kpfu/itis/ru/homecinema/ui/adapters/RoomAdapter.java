package kpfu.itis.ru.homecinema.ui.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import kpfu.itis.ru.homecinema.R;
import kpfu.itis.ru.homecinema.models.Room;

@Deprecated
public class RoomAdapter extends BaseAdapter {

    private final Context mContext;
    private List<Room> mRooms;

    public RoomAdapter(Context context, List<Room> rooms) {
        mContext = context;
        mRooms = rooms == null ? new ArrayList<Room>() : rooms;
    }

    @Override
    public int getCount() {
        return mRooms.size();
    }

    @Override
    public Object getItem(int position) {
        return mRooms.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) convertView = View.inflate(mContext, R.layout.room_item, null);
        TextView nameTextView = (TextView) convertView.findViewById(R.id.tv_name);
//        TextView currentTimeTextView = (TextView) convertView.findViewById(R.id.tv_current_time);
        nameTextView.setText(mRooms.get(position).getName());
        int currentTime = mRooms.get(position).getCurrentTime();
//        if (currentTime > 0) currentTimeTextView.setText(mContext.getString(R.string.current_time_template, currentTime));
//        else currentTimeTextView.setVisibility(View.GONE);
        return convertView;
    }
}
