package kpfu.itis.ru.homecinema.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import kpfu.itis.ru.homecinema.R;
import kpfu.itis.ru.homecinema.models.Message;
import kpfu.itis.ru.homecinema.models.Room;
import kpfu.itis.ru.homecinema.services.MoviesService;
import kpfu.itis.ru.homecinema.ui.adapters.MessageAdapter;
import kpfu.itis.ru.homecinema.ui.custom.MediaContollerView;
import kpfu.itis.ru.homecinema.utils.AppPreferences;

public class MovieViewV2Activity extends AppCompatActivity implements SurfaceHolder.Callback, MediaPlayer.OnPreparedListener,
        MediaContollerView.MediaPlayerControl, MediaContollerView.ControllerBot {

    @Bind(R.id.videoSurface) SurfaceView mVideoView;
    @Bind(R.id.videoSurfaceContainer) FrameLayout mVideoContainer;
    @Bind(R.id.rv_messages) RecyclerView mMessagesRecyclerView;
    @Bind(R.id.btn_send) FloatingActionButton mSendButton;
    @Bind(R.id.et_message) EditText mMessageEditText;

    private MediaPlayer mPlayer;
    private MediaContollerView mController;

    private boolean mCanControl;
    private String mNickname;
    private String mAvatar;
    private Room mRoom;
    private Socket mSocket;
    private String mUrl;
    private boolean mFullscreen;
    private int mBufferPercentage;
    private MessageAdapter mAdapter;
    private List<Message> mMessages;
    private Timer mSyncTimer;
    private Timer mControlTimer;
    private Timer mTimeExpirationTimer;
    private boolean mTimeExpired;
    private int mClickCount;
    private boolean mExpirationTimerStarted;

    private boolean mBuffered = false;
    private static final String URL = "http://172.20.10.5:4000/";

    private static final String SOCKET_TAG = "SOCKET";

    public static final String ACTION_GET_TIME = "getTime";
    private static final String EVENT_GET_TIME = ACTION_GET_TIME;
    private static final String EVENT_SET_TIME = "setTime";
    private static final String EVENT_ROOM = "room";
    private static final String EVENT_SYNC_REQUEST = "sync_request";
    private static final String EVENT_SYNC = "sync";
    private static final String EVENT_SYNC_RESPONSE = "sync_response";
    private static final String EVENT_CHAT_MESSAGE = "chat_message";

    public static final String FILTER = "kpfu.itis.ru.homecinema.ui.MovieViewV2Activity.FILTER";
    public static final String EXTRA_CAN_CONTROL = "kpfu.itis.ru.homecinema.ui.MovieViewV2Activity.extra.CAN_CONTROL";
    public static final String EXTRA_NICKNAME = "kpfu.itis.ru.homecinema.ui.MovieViewV2Activity.extra.NICKNAME";
    public static final String EXTRA_AVATAR = "kpfu.itis.ru.homecinema.ui.MovieViewV2Activity.extra.AVATAR";

    private static final String EVENT_JOIN = "join";

    public static final String EVENT_PLAY_PAUSE = "play_pause";
    public static final String EVENT_SEEK_TO = "seek_to";

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.hasExtra(EXTRA_CAN_CONTROL)) {
                mCanControl = intent.getBooleanExtra(EXTRA_CAN_CONTROL, false);
                mNickname = intent.getStringExtra(EXTRA_NICKNAME);
                mAvatar = intent.getStringExtra(EXTRA_AVATAR);
                createAdapter();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_view_v2);
        ButterKnife.bind(this);
        SurfaceHolder holder = mVideoView.getHolder();
        holder.addCallback(this);

        Intent intent = getIntent();
        if (intent != null) {
            mRoom = (Room) intent.getSerializableExtra(MovieDetailActivity.EXTRA_ROOM);
            mUrl = intent.getStringExtra(MovieDetailActivity.EXTRA_URL);
        }
        createConnection();
        mPlayer = new MediaPlayer();
        mController = new MediaContollerView(this, mSocket);
        String accessToken = AppPreferences.Auth.getAccessToken(this);
        if (accessToken == null) {
            mCanControl = false;
            mNickname = "Anonymous_" + UUID.randomUUID().toString();
            createAdapter();
        }
        else MoviesService.startActionCanControl(this, accessToken);
        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, new IntentFilter(FILTER));
//        createAdapter();
    }

    @Override
    protected void onPause() {
        super.onPause();
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    private void createAdapter() {
        mMessages = new ArrayList<>();
        mAdapter = new MessageAdapter(this, mMessages, mNickname);
        LinearLayoutManager manager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, true);
        mMessagesRecyclerView.setLayoutManager(manager);
        mMessagesRecyclerView.setAdapter(mAdapter);
    }

    @OnClick(R.id.btn_send)
    void onSendButtonClick() {
        if (TextUtils.isEmpty(mMessageEditText.getText())) {
            Toast.makeText(this, R.string.et_message, Toast.LENGTH_LONG).show();
            return;
        }
        if (mMessages == null) mMessages = new ArrayList<>();
        Message message = new Message();
        message.setAuthor(mNickname);
        message.setAvatar(mAvatar);
        message.setText(mMessageEditText.getText().toString());
        message.setDate(new Date());
        mAdapter.addMessage(message);
        sendMessage(message);
        mMessagesRecyclerView.scrollToPosition(0);
        mMessageEditText.setText("");

    }

    private void sendMessage(Message message) {
        Gson gson = new Gson();
        String json = gson.toJson(message);
        if (mSocket != null) mSocket.emit(EVENT_CHAT_MESSAGE, json);
    }

    private void receiveMessage(final Message message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mAdapter != null) mAdapter.addMessage(message);
            }
        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mController.show();
        return false;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        mPlayer.setDisplay(holder);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    @Override
    public void start() {
        mPlayer.start();
    }

    @Override
    public void pause() {
        mPlayer.pause();
    }

    @Override
    public int getDuration() {
        return mPlayer.getDuration();
    }

    @Override
    public int getCurrentPosition() {
        return mPlayer.getCurrentPosition();
    }

    @Override
    public void seekTo(int position) {
        mPlayer.seekTo(position);
    }

    @Override
    public boolean isPlaying() {
        return mPlayer.isPlaying();
    }

    @Override
    public int getBufferPercentage() {
        return mBufferPercentage;
    }

    @Override
    public boolean canPause() {
        return mCanControl;
    }

    @Override
    public boolean canSeekBackward() {
        return mCanControl;
    }

    @Override
    public boolean canSeekForward() {
        return mCanControl;
    }

    @Override
    public boolean isFullScreen() {
        return mFullscreen;
    }

    @Override
    public void toggleFullscreen() {
        DisplayMetrics metrics = new DisplayMetrics();
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mVideoContainer.getLayoutParams();
        if (!mFullscreen) {
            hideSystemUI();
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
            getWindowManager().getDefaultDisplay().getMetrics(metrics);
            params.width = metrics.widthPixels;
            params.height = metrics.heightPixels;
            params.setMargins(0, 0, 0, 0);
        } else {
            showSystemUI();
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            getWindowManager().getDefaultDisplay().getMetrics(metrics);
            params.width = metrics.widthPixels;
            params.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 300, metrics);
            params.setMargins(0, 0, 0, 0);
        }
        mVideoContainer.setLayoutParams(params);
        mFullscreen = !mFullscreen;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mController.setMediaPlayer(this);
        mController.setBot(this);
        mController.setAnchorView((FrameLayout) findViewById(R.id.videoSurfaceContainer));
        mPlayer.start();
        mPlayer.pause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mSyncTimer != null) mSyncTimer.cancel();
        if (mSocket != null) mSocket.disconnect();
        mSocket = null;
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPlayer.pause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mBuffered) mPlayer.start();
    }

    private void hideSystemUI() {
        // BEGIN_INCLUDE (get_current_ui_flags)
        // The UI options currently enabled are represented by a bitfield.
        // getSystemUiVisibility() gives us that bitfield.
        int uiOptions = getWindow().getDecorView().getSystemUiVisibility();
        int newUiOptions = uiOptions;
        // END_INCLUDE (get_current_ui_flags)
        // BEGIN_INCLUDE (toggle_ui_flags)
        boolean isImmersiveModeEnabled =
                ((uiOptions | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY) == uiOptions);
        if (isImmersiveModeEnabled) {
            Log.i("tag", "Turning immersive mode mode off. ");
        } else {
            Log.i("tag", "Turning immersive mode mode on.");
        }

        // Navigation bar hiding:  Backwards compatible to ICS.
        if (Build.VERSION.SDK_INT >= 14) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        }

        // Status bar hiding: Backwards compatible to Jellybean
        if (Build.VERSION.SDK_INT >= 16) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_FULLSCREEN;
        }

        // Immersive mode: Backward compatible to KitKat.
        // Note that this flag doesn't do anything by itself, it only augments the behavior
        // of HIDE_NAVIGATION and FLAG_FULLSCREEN.  For the purposes of this sample
        // all three flags are being toggled together.
        // Note that there are two immersive mode UI flags, one of which is referred to as "sticky".
        // Sticky immersive mode differs in that it makes the navigation and status bars
        // semi-transparent, and the UI flag does not get cleared when the user interacts with
        // the screen.
        if (Build.VERSION.SDK_INT >= 18) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }

        getWindow().getDecorView().setSystemUiVisibility(newUiOptions);
        //END_INCLUDE (set_ui_flags)
    }

    private void showSystemUI() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    private void createConnection() {
        try {
            mSocket = IO.socket(URL);
            mSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {

                }
            }).on(Socket.EVENT_MESSAGE, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    if (args != null && args.length > 0) {
                        final JSONObject json = (JSONObject) args[0];
                        try {
                            if (json.has("url")) {
                                Log.i(SOCKET_TAG, "url: " + json.getString("url") + "; time: " + json.getInt("time"));
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            play(URL + mUrl, json.getInt("time"));
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {

                }
            }).on(EVENT_GET_TIME, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    int currentTime = mPlayer.getCurrentPosition() / 1000;
                    Log.d("tag", currentTime + " is current time; current position is: " + mPlayer.getCurrentPosition());
                    mSocket.emit(EVENT_SET_TIME, currentTime);
                }
            }).on(EVENT_ROOM, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    mSocket.emit(EVENT_JOIN, mRoom.getId());
                }
            }).on(EVENT_PLAY_PAUSE, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    if (args != null && args.length > 0 && args[0] instanceof Boolean) {
                        Boolean playing = (Boolean) args[0];
                        playOrPause(playing);
                    }
                }
            }).on(EVENT_SEEK_TO, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.d(SOCKET_TAG, "EVENT_SEEK_TO");
                    if (args != null && args.length > 0 && args[0] instanceof Integer) {
                        Integer position = (Integer) args[0];
                        Log.d(SOCKET_TAG, "position is " + position);
                        goTo(position);
                    }
                }
            }).on(EVENT_SYNC_REQUEST, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.d(SOCKET_TAG, args[0].toString());
                    String socketId = args[0].toString();
                    int currentTime = mPlayer.getCurrentPosition() / 1000;
                    mSocket.emit(EVENT_SYNC, currentTime, socketId);
                }
            }).on(EVENT_SYNC_RESPONSE, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    int currentTime = mPlayer.getCurrentPosition() / 1000;
                    int timeFromServer = (Integer) args[0];
                    if (Math.abs(timeFromServer - currentTime) > 5) {
                        mPlayer.seekTo(timeFromServer * 1000);
                    }
                }
            }).on(EVENT_CHAT_MESSAGE, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    if (args == null || args.length == 0 || args[0] == null) return;
                    Gson gson = new Gson();
                    Message message = gson.fromJson(args[0].toString(), Message.class);
                    message.setDate(new Date());
                    receiveMessage(message);
                }
            });
            mSocket.connect();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    private void goTo(Integer position) {
        mPlayer.seekTo(position);
    }

    private void playOrPause(Boolean playing) {
        if (playing) mPlayer.pause();
        else mPlayer.start();
    }

    private void play(String url, int time) {
        if (!mBuffered) {
            String playingUrl = URL + mUrl;
            try {
                mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mPlayer.setDataSource(this, Uri.parse(playingUrl));
                mPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
                    @Override
                    public void onBufferingUpdate(MediaPlayer mp, int percent) {
                        mBufferPercentage = percent;
                        if (percent > 3) {
                            if (!mBuffered) {
                                Log.d(SOCKET_TAG, "percent is: " + percent);
                                mBuffered = true;

                                mSocket.emit(EVENT_GET_TIME, mRoom.getId());
                            }
                        }
                    }
                });
                mPlayer.setOnPreparedListener(this);
                mPlayer.prepareAsync();
                mPlayer.seekTo(time * 1000);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Log.d(SOCKET_TAG, "mBuffered");
            if (time != 0) mPlayer.seekTo(time * 1000);
            mPlayer.start();
            mSyncTimer = new Timer();
            mSyncTimer.schedule(new TimeRequestTask(), 300000, 300000);
        }
    }

    @Override
    public void onControlPressed() {
        if (!mTimeExpired && mExpirationTimerStarted) mClickCount++;
        if (!mExpirationTimerStarted && mCanControl) {
            mExpirationTimerStarted = true;
            mTimeExpirationTimer = new Timer();
            mTimeExpirationTimer.schedule(new TimeExpirationTask(), 20000);
        }
        if (mClickCount > 5) {
            Toast.makeText(MovieViewV2Activity.this, R.string.control_denied, Toast.LENGTH_LONG).show();
            if (mTimeExpirationTimer != null) {
                mTimeExpirationTimer.cancel();
                mExpirationTimerStarted = false;
            }
            mControlTimer = new Timer();
            mCanControl = false;
            mControlTimer.schedule(new ControlTask(), 600000);
            mClickCount = 0;
        }
    }

    @Override
    public boolean isTimeExpired() {
        return mTimeExpired;
    }

    class TimeRequestTask extends TimerTask {

        @Override
        public void run() {
            mSocket.emit(EVENT_SYNC_REQUEST, mRoom.getId());
        }
    }

    class ControlTask extends TimerTask {

        @Override
        public void run() {
            mCanControl = true;
        }
    }

    class TimeExpirationTask extends TimerTask {

        @Override
        public void run() {
            mTimeExpired = true;
            mExpirationTimerStarted = false;
        }
    }

}
