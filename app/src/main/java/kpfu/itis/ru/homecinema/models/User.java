package kpfu.itis.ru.homecinema.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class User implements Serializable {

    @SerializedName("login")
    @Expose
    private String mLogin;

    @SerializedName("email")
    @Expose
    private String mEmail;

    @SerializedName("avatar")
    @Expose
    private String mAvatar;

    @SerializedName("isDisseminator")
    @Expose
    private boolean mDisseminator;

    public String getLogin() {
        return mLogin;
    }

    public void setLogin(String login) {
        mLogin = login;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getAvatar() {
        return mAvatar;
    }

    public void setAvatar(String avatar) {
        mAvatar = avatar;
    }

    public boolean isDisseminator() {
        return mDisseminator;
    }

    public void setDisseminator(boolean disseminator) {
        mDisseminator = disseminator;
    }
}
