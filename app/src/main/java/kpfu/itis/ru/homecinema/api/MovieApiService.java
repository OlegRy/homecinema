package kpfu.itis.ru.homecinema.api;

import java.util.List;

import kpfu.itis.ru.homecinema.models.CanControlResponse;
import kpfu.itis.ru.homecinema.models.GenresResponse;
import kpfu.itis.ru.homecinema.models.Movie;
import kpfu.itis.ru.homecinema.models.Room;
import kpfu.itis.ru.homecinema.models.Token;
import kpfu.itis.ru.homecinema.models.User;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface MovieApiService {

    @GET("/movies")
    Call<List<Movie>> getMovies(@Query(value = "genre", encoded = true) String genre);

    @GET("/roomExists")
    Call<ResponseBody> roomExists(@Query(value = "movie_id", encoded = true) int movieId, @Query(value = "room_name", encoded = true) String roomName);

    @POST("/createRoom")
    Call<Room> createRoom(@Body Room room);

    @POST("/signin")
    Call<Token> signIn(@Query(value = "login", encoded = true) String login, @Query(value = "password", encoded = true) String password);

    @POST("/signup")
    Call<ResponseBody> signUp(@Query(value = "login", encoded = true) String login, @Query(value = "password", encoded = true) String password, @Query(value = "email", encoded = true) String email,
                              @Query(value = "avatar", encoded = true) String avatar, @Query("isDisseminator") boolean disseminator);

    @GET("/getUser")
    Call<User> getUser(@Header("Authorization") String accessToken);

    @POST("/createMovie")
    Call<ResponseBody> createMovie(@Body Movie movie);

    @PUT("/changePassword")
    Call<ResponseBody> changePassword(@Header("Authorization") String accessToken, @Query(value = "login", encoded = true) String login,
                                      @Query(value = "currentPassword", encoded = true) String currentPassword,
                                      @Query(value = "newPassword", encoded = true) String newPassword);

    @GET("/genres")
    Call<GenresResponse> genres();

    @GET("/canControl")
    Call<CanControlResponse> canControl(@Header("Authorization") String accessToken);
}
