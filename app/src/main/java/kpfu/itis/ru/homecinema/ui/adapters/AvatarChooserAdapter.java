package kpfu.itis.ru.homecinema.ui.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import kpfu.itis.ru.homecinema.R;

public class AvatarChooserAdapter extends BaseAdapter {

    private CharSequence[] mCases;
    private Context mContext;

    public AvatarChooserAdapter(Context context) {
        mContext = context;
        mCases = context.getResources().getTextArray(R.array.avatar_choice_case);
    }

    @Override
    public int getCount() {
        return mCases.length;
    }

    @Override
    public Object getItem(int position) {
        return mCases[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) convertView = View.inflate(mContext, R.layout.avatar_chooser_item, null);
        ImageView icon = (ImageView) convertView.findViewById(R.id.iv_icon);
        TextView caseTextView = (TextView) convertView.findViewById(R.id.tv_case);

        if (position == 0) icon.setImageResource(R.drawable.ic_camera_alt_black_36dp);
        else icon.setImageResource(R.drawable.ic_image_black_36dp);
        caseTextView.setText(mCases[position]);
        return convertView;
    }
}
