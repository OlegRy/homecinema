package kpfu.itis.ru.homecinema.ui.fragments;


import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kpfu.itis.ru.homecinema.R;
import kpfu.itis.ru.homecinema.services.MoviesService;
import kpfu.itis.ru.homecinema.ui.adapters.AvatarChooserAdapter;
import kpfu.itis.ru.homecinema.utils.FileUtil;
import kpfu.itis.ru.homecinema.utils.UIUtil;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignUpFragment extends Fragment {

    public static final String SUCCESS_MESSAGE = "kpfu.itis.ru.homecinema.fragments.SignUpFragment.SUCCESS";
    public static final String ERROR_MESSAGE = "kpfu.itis.ru.homecinema.fragments.SignUpFragment.ERROR";
    public static final String FILTER = "kpfu.itis.ru.homecinema.fragments.SignUpFragment.FILTER";

    private static final String PICTURE_PATTERN = ".*\\.(jpg|png|bmp|JPG|PNG|BMP)";
    private static final int REQUEST_FILE = 100;

    private static final int TAKE_PICTURE_ACTION = 0;
    private static final int CHOOSE_FROM_GALLERY_ACTION = 1;
    private static final int PERMISSION_STORAGE = 100;

    private ViewHolder mViewHolder;

    private MaterialDialog mProgressDialog;

    private String mAvatarPath;

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (mProgressDialog != null) mProgressDialog.dismiss();
            if (intent.hasExtra(ERROR_MESSAGE)) {
                showMessage(R.string.error_title, intent.getStringExtra(ERROR_MESSAGE));
            } else if (intent.hasExtra(SUCCESS_MESSAGE)) {
                showMessage(R.string.success_title, intent.getStringExtra(SUCCESS_MESSAGE));
                completeSignUp();
            }
        }
    };

    private void showMessage(@StringRes int title, String message) {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(getContext());
        builder.title(title);
        builder.content(message);
        builder.positiveText(R.string.ok);
        builder.show();
    }

    class ViewHolder {

        @Bind(R.id.iv_avatar) ImageView mAvatarImageView;
        @Bind(R.id.iv_avatar_capture) ImageView mAvatarCaptureImageView;
        @Bind(R.id.et_login) EditText mLoginEditText;
        @Bind(R.id.et_password) EditText mPasswordEditText;
        @Bind(R.id.et_password_confirmation) EditText mPasswordConfirmationEditText;
        @Bind(R.id.et_email) EditText mEmailEditText;
        @Bind(R.id.cb_disseminator) CheckBox mDisseminatorCheckBox;
        @Bind(R.id.tv_complete)TextView mCompleteTextView;

        @OnClick(R.id.tv_complete)
        void onCompleteTextViewClick() {
            checkFieldsAndStartSigningUp();
        }

        @OnClick(R.id.iv_avatar_capture)
        void onAvatarCaptureImageViewClick() {
            requestStoragePermission();
        }
    }


    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            openAvatarDialog();
        } else {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_STORAGE);
        }
    }

    private void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, CHOOSE_FROM_GALLERY_ACTION);
    }

    private void openCamera() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(getContext().getPackageManager()) != null) startActivityForResult(cameraIntent, TAKE_PICTURE_ACTION);
    }

    private void openAvatarDialog() {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(getContext());
        builder.title(R.string.choose_avatar_title)
                .adapter(new AvatarChooserAdapter(getContext()),
                        new MaterialDialog.ListCallback() {
                            @Override
                            public void onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                                switch (which) {
                                    case TAKE_PICTURE_ACTION:
                                        openCamera();
                                        break;
                                    case CHOOSE_FROM_GALLERY_ACTION:
                                        openGallery();
                                        break;
                                }
                                dialog.dismiss();
                            }
                        })
                .negativeText(R.string.cancel)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    private void checkFieldsAndStartSigningUp() {
        if (noErrors()) {
            String login = mViewHolder.mLoginEditText.getText().toString();
            String password = mViewHolder.mPasswordEditText.getText().toString();
            String email = mViewHolder.mEmailEditText.getText().toString();
            boolean disseminator = mViewHolder.mDisseminatorCheckBox.isChecked();
            mProgressDialog = UIUtil.showProgressDialog(getContext());
            MoviesService.startActionSignUp(getContext(), login, password, email, mAvatarPath, disseminator);
        }
    }

    private boolean noErrors() {
        if (mViewHolder.mLoginEditText.getText() == null || TextUtils.isEmpty(mViewHolder.mLoginEditText.getText())) {
            showMessage(R.string.error_title, getString(R.string.login_empty_error));
            return false;
        }
        if (mViewHolder.mLoginEditText.getText().toString().length() < 4) {
            showMessage(R.string.error_title, getString(R.string.login_length_error));
            return false;
        }
        if (mViewHolder.mPasswordEditText.getText() == null || TextUtils.isEmpty(mViewHolder.mPasswordEditText.getText())) {
            showMessage(R.string.error_title, getString(R.string.password_empty_error));
            return false;
        }
        if (mViewHolder.mPasswordConfirmationEditText.getText() == null || TextUtils.isEmpty(mViewHolder.mPasswordConfirmationEditText.getText())) {
            showMessage(R.string.error_title, getString(R.string.password_confirmation_empty_error));
            return false;
        }
        if (!mViewHolder.mPasswordEditText.getText().toString().equals(mViewHolder.mPasswordConfirmationEditText.getText().toString())) {
            showMessage(R.string.error_title, getString(R.string.password_and_confirmation_not_same_error));
            return false;
        }
        if (mViewHolder.mEmailEditText.getText() == null || TextUtils.isEmpty(mViewHolder.mEmailEditText.getText())) {
            showMessage(R.string.error_title, getString(R.string.email_empty_error));
            return false;
        }
        if (!isEmail(mViewHolder.mEmailEditText.getText().toString())) {
            showMessage(R.string.error_title, getString(R.string.email_incorrect_error));
            return false;
        }
        return true;
    }

    private boolean isEmail(String potentialEmail) {
        Pattern pattern = Pattern.compile("^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}$");
        Matcher matcher = pattern.matcher(potentialEmail);
        return matcher.matches();
    }

    private void completeSignUp() {
        String login = mViewHolder.mLoginEditText.getText().toString();
        String password = mViewHolder.mPasswordEditText.getText().toString();
        AuthFragment fragment = AuthFragment.newInstance(login, password);
        getFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
    }

    public SignUpFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = ((AppCompatActivity) getContext()).getSupportActionBar();
        actionBar.setTitle(R.string.tv_sign_up);
        actionBar.show();

        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mReceiver, new IntentFilter(FILTER));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mReceiver);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_up, container, false);
        mViewHolder = new ViewHolder();
        ButterKnife.bind(mViewHolder, view);
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == getActivity().RESULT_OK) {
            if (requestCode == TAKE_PICTURE_ACTION || requestCode == CHOOSE_FROM_GALLERY_ACTION) {
                addAvatar(data);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_STORAGE:
                if (grantResults != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openAvatarDialog();
                } else {
                    Toast.makeText(getContext(), "No permission", Toast.LENGTH_SHORT).show();
                }
        }
    }

    private void addAvatar(Intent data) {
        Uri uri = data.getData();
        mAvatarPath = FileUtil.convertToPath(getContext(), uri);
        loadBitmap(uri);
    }

    private void loadBitmap(Uri uri) {
        Glide.with(getContext())
                .load(uri)
                .asBitmap()
                .centerCrop()
                .into(mViewHolder.mAvatarImageView);
    }
}
