package kpfu.itis.ru.homecinema.ui;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nbsp.materialfilepicker.MaterialFilePicker;
import com.nbsp.materialfilepicker.ui.FilePickerActivity;

import java.io.File;
import java.io.IOException;
import java.util.regex.Pattern;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kpfu.itis.ru.homecinema.R;
import kpfu.itis.ru.homecinema.models.Movie;
import kpfu.itis.ru.homecinema.models.User;
import kpfu.itis.ru.homecinema.services.MoviesService;
import kpfu.itis.ru.homecinema.ui.adapters.AvatarChooserAdapter;
import kpfu.itis.ru.homecinema.utils.AppPreferences;
import kpfu.itis.ru.homecinema.utils.FileUtil;
import kpfu.itis.ru.homecinema.utils.UIUtil;

public class HomeActivity extends AppCompatActivity {

    private static final int TAKE_PICTURE_ACTION = 0;
    private static final int CHOOSE_FROM_GALLERY_ACTION = 1;
    private static final int REQUEST_FILE = 100;
    private static final int PERMISSION_STORAGE_PICTURE = 11;
    private static final int PERMISSION_STORAGE_VIDEO = 10;
    private static final int PERMISSION_CAMERA = 12;
    private static final int PERMISSION_STORAGE = 13;

    public static final String FILTER = "kpfu.itis.ru.homecinema.ui.HomeActivity.FILTER";
    public static final String EXTRA_MOVIE_CREATED_MESSAGE = "kpfu.itis.ru.homecinema.ui.HomeActivity.extra.MESSAGE";
    public static final String EXTRA_CHANGE_PASSWORD_MESSAGE = "kpfu.itis.ru.homecinema.ui.HomeActivity.extra.CHANGE_PASSWORD_MESSAGE";

    private ActivityViewHolder mActivityViewHolder;
    private UploadDialogViewHolder mUploadDialogViewHolder;

    private MaterialDialog mProgressDialog;

    private User mUser;
    private String mPosterPath;
    private String mVideoPath;

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (mProgressDialog != null && mProgressDialog.isShowing()) mProgressDialog.dismiss();
            if (intent.hasExtra(EXTRA_MOVIE_CREATED_MESSAGE)) {
                showMessage(intent.getStringExtra(EXTRA_MOVIE_CREATED_MESSAGE));
            } else if (intent.hasExtra(EXTRA_CHANGE_PASSWORD_MESSAGE)) {
                showMessage(intent.getStringExtra(EXTRA_CHANGE_PASSWORD_MESSAGE));
            }
        }
    };

    private void showMessage(String message) {
        UIUtil.showMessage(this, message);
    }

    class ActivityViewHolder {
        @Bind(R.id.toolbar) Toolbar mToolbar;
        @Bind(R.id.iv_avatar) ImageView mAvatarImageView;
    }

    class UploadDialogViewHolder {
        @Bind(R.id.iv_poster) ImageView mPosterImageView;
        @Bind(R.id.iv_poster_capture) ImageView mPosterCaptureImageView;
        @Bind(R.id.et_title) EditText mTitleEditText;
        @Bind(R.id.et_description) EditText mDescriptionEditText;
        @Bind(R.id.et_duration) EditText mDurationEditText;
        @Bind(R.id.et_genre) EditText mGenreEditText;
        @Bind(R.id.tv_attach_file) TextView mAttachFileTextView;

        @OnClick(R.id.tv_attach_file)
        void onAttachFileTextViewClick() {
            requestStoragePermissionForVideo();
        }

        @OnClick(R.id.iv_poster_capture)
        void onPosterCaptureImageViewClick() {
            requestStoragePermissionForPicture();
        }
    }

    private void openAvatarDialog() {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this);
        builder.title(R.string.choose_avatar_title)
                .adapter(new AvatarChooserAdapter(this),
                        new MaterialDialog.ListCallback() {
                            @Override
                            public void onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                                switch (which) {
                                    case TAKE_PICTURE_ACTION:
                                        requestCameraPermission();
                                        break;
                                    case CHOOSE_FROM_GALLERY_ACTION:
                                        requestStoragePermission();
                                        break;
                                }
                                dialog.dismiss();
                            }
                        })
                .negativeText(R.string.cancel)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    private void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, CHOOSE_FROM_GALLERY_ACTION);
    }

    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            openGallery();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_STORAGE);
        }
    }

    private void requestCameraPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            openCamera();
        } else {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.CAMERA}, PERMISSION_CAMERA);
        }
    }

    private void openCamera() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(getPackageManager()) != null) startActivityForResult(cameraIntent, TAKE_PICTURE_ACTION);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Intent intent = getIntent();
        if (intent.hasExtra(MoviesActivity.EXTRA_USER)) mUser = (User) intent.getSerializableExtra(MoviesActivity.EXTRA_USER);
        mActivityViewHolder = new ActivityViewHolder();
        ButterKnife.bind(mActivityViewHolder, this);
        setSupportActionBar(mActivityViewHolder.mToolbar);
        if (mUser != null) {
            getSupportActionBar().setTitle(mUser.getLogin());
            if (mUser.getAvatar() != null) {
                loadAvatar();
            }
        }
    }

    private void loadAvatar() {
        Glide.with(this)
                .load(getString(R.string.base_url) + "/" +  mUser.getAvatar())
                .asBitmap()
                .skipMemoryCache(false)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .centerCrop()
                .into(mActivityViewHolder.mAvatarImageView);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.me_movies:
                openMovies();
                return true;
            case R.id.me_upload:
                openUploadDialog();
                return true;
            case R.id.me_logout:
                logout();
                return true;
            case R.id.me_change_avatar:
                openAvatarDialog();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_STORAGE_VIDEO:
                if (grantResults != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    requestStoragePermissionForVideo();
                } else {
                    Toast.makeText(this, "No permission", Toast.LENGTH_SHORT).show();
                }
                break;
            case PERMISSION_STORAGE_PICTURE:
                if (grantResults != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    requestStoragePermissionForPicture();
                } else {
                    Toast.makeText(this, "No permission", Toast.LENGTH_SHORT).show();
                }
                break;
            case PERMISSION_CAMERA:
                if (grantResults != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openCamera();
                } else {
                    Toast.makeText(this, "No permission", Toast.LENGTH_SHORT).show();
                }
                break;
            case PERMISSION_STORAGE:
                if (grantResults != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openGallery();
                } else {
                    Toast.makeText(this, "No permission", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void requestStoragePermissionForVideo() {
        requestStoragePermission(Pattern.compile(".*\\.(mp4|MP4)"), PERMISSION_STORAGE_VIDEO);
    }

    private void requestStoragePermissionForPicture() {
        requestStoragePermission(Pattern.compile(".*\\.(jpg|png|bmp|JPG|PNG|BMP)"), PERMISSION_STORAGE_PICTURE);
    }

    private void requestStoragePermission(Pattern pattern, int requestCode) {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            chooseFile(pattern);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, requestCode);
        }
    }

    private void chooseFile(Pattern pattern) {
        new MaterialFilePicker()
                .withActivity(this)
                .withFilter(pattern)
                .withRequestCode(REQUEST_FILE)
                .start();
    }

    private void logout() {
        AppPreferences.clear(this);
        Intent intent = new Intent(this, HomeCinemaActivity.class);
        startActivity(intent);
        finish();
    }

    private void openMovies() {
        Intent intent = new Intent(this, MoviesActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case TAKE_PICTURE_ACTION:
                    changeAvatarFromCamera(data);
                    break;
                case CHOOSE_FROM_GALLERY_ACTION:
                    changeAvatarFromGallery(data);
                    break;
                case REQUEST_FILE:
                    String path = data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH);
                    if (FileUtil.isPicture(path)) {
                        loadPicture(path);
                        mPosterPath = path;
                    }
                    else {
                        pathToTextView(path);
                        mVideoPath = path;
                    }
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);

    }

    private void pathToTextView(String path) {
        mUploadDialogViewHolder.mAttachFileTextView.setText(path);
    }

    private void loadPicture(String path) {
        Glide.with(this)
                .load(new File(path))
                .asBitmap()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .centerCrop()
                .into(mUploadDialogViewHolder.mPosterImageView);
    }

    private void openUploadDialog() {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this);
        builder.title(R.string.upload_video);
        builder.customView(R.layout.dialog_create_movie, true);
        builder.positiveText(R.string.upload);
        builder.negativeText(R.string.cancel);
        builder.onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                uploadFile();
            }
        });
        builder.onNegative(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                dialog.dismiss();
            }
        });
        MaterialDialog dialog = builder.build();
        View view = dialog.getCustomView();
        mUploadDialogViewHolder = new UploadDialogViewHolder();
        ButterKnife.bind(mUploadDialogViewHolder, view);
        dialog.show();

    }

    private void uploadFile() {
        if (isDataCorrect()) {
            Movie movie = new Movie();
            movie.setTitle(mUploadDialogViewHolder.mTitleEditText.getText().toString());
            if (mUploadDialogViewHolder.mDescriptionEditText.getText() != null && !TextUtils.isEmpty(mUploadDialogViewHolder.mDescriptionEditText.getText())) {
                movie.setDescription(mUploadDialogViewHolder.mDescriptionEditText.getText().toString());
            }
            movie.setDuration(Integer.parseInt(mUploadDialogViewHolder.mDurationEditText.getText().toString()));
            movie.setGenre(mUploadDialogViewHolder.mGenreEditText.getText().toString());
            MoviesService.startActionUploadFile(this, mVideoPath, movie, mPosterPath);
        }
    }

    private boolean isDataCorrect() {
        if (mUploadDialogViewHolder.mTitleEditText.getText() == null || TextUtils.isEmpty(mUploadDialogViewHolder.mTitleEditText.getText())) {
            Toast.makeText(this, R.string.movie_title_empty_error, Toast.LENGTH_LONG).show();
            return false;
        }
        if (mUploadDialogViewHolder.mDurationEditText.getText() == null || TextUtils.isEmpty(mUploadDialogViewHolder.mDurationEditText.getText())) {
            Toast.makeText(this, R.string.duration_empty_error, Toast.LENGTH_LONG).show();
            return false;
        }
        if (mVideoPath == null || "".equals(mVideoPath)) {
            Toast.makeText(this, R.string.video_path_empty_error, Toast.LENGTH_LONG).show();
            return false;
        }
        if (mUploadDialogViewHolder.mGenreEditText.getText() == null || TextUtils.isEmpty(mUploadDialogViewHolder.mGenreEditText.getText())) {
            Toast.makeText(this, R.string.genre_empty_error, Toast.LENGTH_LONG).show();
            return false;
        }
        return true;

    }

    private void changeAvatarFromGallery(Intent data) {
        Uri avatarUri = data.getData();
        try {
            Bitmap avatar = MediaStore.Images.Media.getBitmap(getContentResolver(), avatarUri);
            loadBitmap(avatar);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void changeAvatarFromCamera(Intent data) {
        Bundle extras = data.getExtras();
        Bitmap avatar = (Bitmap) extras.get("data");
        loadBitmap(avatar);
    }

    private void loadBitmap(Bitmap bitmap) {
        mActivityViewHolder.mAvatarImageView.setImageBitmap(bitmap);
    }
}
