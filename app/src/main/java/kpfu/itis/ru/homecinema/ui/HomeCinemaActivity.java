package kpfu.itis.ru.homecinema.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import butterknife.Bind;
import butterknife.ButterKnife;
import kpfu.itis.ru.homecinema.R;
import kpfu.itis.ru.homecinema.ui.fragments.AuthFragment;

public class HomeCinemaActivity extends AppCompatActivity {

    private ViewHolder mViewHolder;

    class ViewHolder {
        @Bind(R.id.toolbar) Toolbar mToolbar;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_cinema);
        mViewHolder = new ViewHolder();
        ButterKnife.bind(mViewHolder, this);
        setSupportActionBar(mViewHolder.mToolbar);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.container, new AuthFragment())
                .commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (getSupportFragmentManager().findFragmentById(R.id.container) instanceof AuthFragment) {
            getSupportActionBar().hide();
        }
    }
}
