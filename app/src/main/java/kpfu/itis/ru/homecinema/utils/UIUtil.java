package kpfu.itis.ru.homecinema.utils;

import android.content.Context;

import com.afollestad.materialdialogs.MaterialDialog;

import kpfu.itis.ru.homecinema.R;

public class UIUtil {

    public static MaterialDialog showProgressDialog(Context context) {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(context);
        builder.title(R.string.app_name);
        builder.content(R.string.progress_content);
        builder.cancelable(false);
        builder.progress(true, 0);
        return builder.show();
    }

    public static MaterialDialog showMessage(Context context, String message) {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(context);
        builder.title(R.string.app_name);
        builder.content(message);
        builder.positiveText(R.string.ok);
        return builder.show();
    }
}
