package kpfu.itis.ru.homecinema.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import kpfu.itis.ru.homecinema.R;
import kpfu.itis.ru.homecinema.api.Helper;
import kpfu.itis.ru.homecinema.api.MovieApiService;
import kpfu.itis.ru.homecinema.models.CanControlResponse;
import kpfu.itis.ru.homecinema.models.GenresResponse;
import kpfu.itis.ru.homecinema.models.Movie;
import kpfu.itis.ru.homecinema.models.Room;
import kpfu.itis.ru.homecinema.models.Token;
import kpfu.itis.ru.homecinema.models.User;
import kpfu.itis.ru.homecinema.ui.HomeActivity;
import kpfu.itis.ru.homecinema.ui.MovieDetailActivity;
import kpfu.itis.ru.homecinema.ui.MovieViewV2Activity;
import kpfu.itis.ru.homecinema.ui.MoviesActivity;
import kpfu.itis.ru.homecinema.ui.fragments.AuthFragment;
import kpfu.itis.ru.homecinema.ui.fragments.MovieListFragment;
import kpfu.itis.ru.homecinema.ui.fragments.SignUpFragment;
import kpfu.itis.ru.homecinema.utils.AppPreferences;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MoviesService extends IntentService {

    private static final String ACTION_GET_MOVIES = "kpfu.itis.ru.homecinema.services.action.GET_MOVIES";
    private static final String ACTION_ROOM_EXISTS = "kpfu.itis.ru.homecinema.services.action.ROOM_EXISTS";
    private static final String ACTION_CREATE_ROOM = "kpfu.itis.ru.homecinema.services.action.CREATE_ROOM";
    private static final String ACTION_UPLOAD_FILE = "kpfu.itis.ru.homecinema.services.action.UPLOAD_FILE";
    private static final String ACTION_SIGN_IN = "kpfu.itis.ru.homecinema.services.action.SIGN_IN";
    private static final String ACTION_SIGN_UP = "kpfu.itis.ru.homecinema.services.action.SIGN_UP";
    private static final String ACTION_GET_USER = "kpfu.itis.ru.homecinema.services.action.GET_USER";
    private static final String ACTION_CHANGE_PASSWORD = "kpfu.itis.ru.homecinema.services.action.CHANGE_PASSWORD";
    private static final String ACTION_GENRES = "kpfu.itis.ru.homecinema.services.action.GENRES";

    private static final String EXTRA_PARAM_MOVIE_ID = "kpfu.itis.ru.homecinema.services.extra.MOVIE_ID";
    private static final String EXTRA_ROOM_NAME = "kpfu.itis.ru.homecinema.services.extra.ROOM_NAME";

    private static final String JSON_ROOM_EXISTS_KEY = "exists";
    public static final String EXTRA_EXISTS = "kpfu.itis.ru.homecinema.services.extra.EXISTS";
    private static final String EXTRA_ROOM = "kpfu.itis.ru.homecinema.services.extra.ROOM";
    private static final String EXTRA_PATH = "kpfu.itis.ru.homecinema.services.extra.PATH";
    private static final String EXTRA_LOGIN = "kpfu.itis.ru.homecinema.services.extra.LOGIN";
    private static final String EXTRA_PASSWORD = "kpfu.itis.ru.homecinema.services.extra.PASSWORD";
    private static final String EXTRA_EMAIL = "kpfu.itis.ru.homecinema.services.extra.EMAIL";
    private static final String EXTRA_TOKEN = "kpfu.itis.ru.homecinema.services.extra.TOKEN";
    private static final String EXTRA_MOVIE = "kpfu.itis.ru.homecinema.services.extra.MOVIE";
    private static final String EXTRA_POSTER = "kpfu.itis.ru.homecinema.services.extra.POSTER";
    private static final String EXTRA_AVATAR = "kpfu.itis.ru.homecinema.services.extra.AVATAR";
    private static final String EXTRA_CURRENT_PASSWORD = "kpfu.itis.ru.homecinema.services.extra.CURRENT_PASSWORD";
    private static final String EXTRA_NEW_PASSWORD = "kpfu.itis.ru.homecinema.services.extra.NEW_PASSWORD";
    private static final String EXTRA_DISSEMINATOR = "kpfu.itis.ru.homecinema.services.extra.DISSEMINATOR";
    private static final String EXTRA_GENRE = "kpfu.itis.ru.homecinema.services.extra.GENRE";
    private static final String ACTION_CAN_CONTROL = "kpfu.itis.ru.homecinema.services.action.CAN_CONTROL";

    private MovieApiService mApi;

    public MoviesService() {
        super("MoviesService");
    }

    /**
     * Starts this service to perform action GetMovies with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void startActionGetMovies(Context context, String genre) {
        Intent intent = new Intent(context, MoviesService.class);
        intent.setAction(ACTION_GET_MOVIES);
        intent.putExtra(EXTRA_GENRE, genre);
        context.startService(intent);
    }

    public static void startActionUploadFile(Context context, String path, Movie movie, String posterPath) {
        Intent intent = new Intent(context, MoviesService.class);
        intent.setAction(ACTION_UPLOAD_FILE);
        intent.putExtra(EXTRA_PATH, path);
        intent.putExtra(EXTRA_MOVIE, movie);
        intent.putExtra(EXTRA_POSTER, posterPath);
        context.startService(intent);
    }

    public static void startActionRoomExists(Context context, int movieId, String roomName) {
        Intent intent = new Intent(context, MoviesService.class);
        intent.setAction(ACTION_ROOM_EXISTS);
        intent.putExtra(EXTRA_PARAM_MOVIE_ID, movieId);
        intent.putExtra(EXTRA_ROOM_NAME, roomName);
        context.startService(intent);
    }

    public static void startActionCreateRoom(Context context, Room room) {
        Intent intent = new Intent(context, MoviesService.class);
        intent.setAction(ACTION_CREATE_ROOM);
        intent.putExtra(EXTRA_ROOM, room);
        context.startService(intent);
    }

    public static void startActionSignIn(Context context, String login, String password) {
        Intent intent = new Intent(context, MoviesService.class);
        intent.setAction(ACTION_SIGN_IN);
        intent.putExtra(EXTRA_LOGIN, login);
        intent.putExtra(EXTRA_PASSWORD, password);
        context.startService(intent);
    }

    public static void startActionSignUp(Context context, String login, String password, String email, String avatarPath, boolean disseminator) {
        Intent intent = new Intent(context, MoviesService.class);
        intent.setAction(ACTION_SIGN_UP);
        intent.putExtra(EXTRA_LOGIN, login);
        intent.putExtra(EXTRA_PASSWORD, password);
        intent.putExtra(EXTRA_EMAIL, email);
        intent.putExtra(EXTRA_AVATAR, avatarPath);
        intent.putExtra(EXTRA_DISSEMINATOR, disseminator);
        context.startService(intent);
    }

    public static void startActionGetUser(Context context, String accessToken) {
        Intent intent = new Intent(context, MoviesService.class);
        intent.setAction(ACTION_GET_USER);
        intent.putExtra(EXTRA_TOKEN, accessToken);
        context.startService(intent);
    }

    public static void startActionChangePassword(Context context, String accessToken, String  login, String currentPassword, String newPassword) {
        Intent intent = new Intent(context, MoviesService.class);
        intent.setAction(ACTION_CHANGE_PASSWORD);
        intent.putExtra(EXTRA_TOKEN, accessToken);
        intent.putExtra(EXTRA_LOGIN, login);
        intent.putExtra(EXTRA_CURRENT_PASSWORD, currentPassword);
        intent.putExtra(EXTRA_NEW_PASSWORD, newPassword);
        context.startService(intent);
    }

    public static void startActionGenres(Context context) {
        Intent intent = new Intent(context, MoviesService.class);
        intent.setAction(ACTION_GENRES);
        context.startService(intent);
    }

    public static void startActionCanControl(Context context, String accessToken) {
        Intent intent = new Intent(context, MoviesService.class);
        intent.setAction(ACTION_CAN_CONTROL);
        intent.putExtra(EXTRA_TOKEN, accessToken);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            switch (action) {
                case ACTION_GET_MOVIES:
                    String genre = intent.getStringExtra(EXTRA_GENRE);
                    handleActionGetMovies(genre);
                    break;
                case ACTION_ROOM_EXISTS:
                    final int movieId = intent.getIntExtra(EXTRA_PARAM_MOVIE_ID, 0);
                    final String roomName = intent.getStringExtra(EXTRA_ROOM_NAME);
                    handleActionRoomExists(movieId, roomName);
                    break;
                case ACTION_CREATE_ROOM:
                    final Room room = (Room) intent.getSerializableExtra(EXTRA_ROOM);
                    handleActionCreateRoom(room);
                    break;
                case ACTION_UPLOAD_FILE:
                    final String path = intent.getStringExtra(EXTRA_PATH);
                    Movie movie = intent.getParcelableExtra(EXTRA_MOVIE);
                    String posterPath = intent.getStringExtra(EXTRA_POSTER);
                    handleActionUploadFile(path, movie, posterPath);
                    break;
                case ACTION_SIGN_IN:
                    String login = intent.getStringExtra(EXTRA_LOGIN);
                    String password = intent.getStringExtra(EXTRA_PASSWORD);
                    handleActionSignIn(login, password);
                    break;
                case ACTION_SIGN_UP:
                    login = intent.getStringExtra(EXTRA_LOGIN);
                    password = intent.getStringExtra(EXTRA_PASSWORD);
                    String email = intent.getStringExtra(EXTRA_EMAIL);
                    String avatar = intent.getStringExtra(EXTRA_AVATAR);
                    boolean disseminator = intent.getBooleanExtra(EXTRA_DISSEMINATOR, false);
                    handleActionSignUp(login, password, email, avatar, disseminator);
                    break;
                case ACTION_GET_USER:
                    String accessToken = intent.getStringExtra(EXTRA_TOKEN);
                    handleActionGetUser(accessToken);
                    break;
                case ACTION_CHANGE_PASSWORD:
                    accessToken = intent.getStringExtra(EXTRA_TOKEN);
                    login = intent.getStringExtra(EXTRA_LOGIN);
                    String currentPassword = intent.getStringExtra(EXTRA_CURRENT_PASSWORD);
                    String newPassword = intent.getStringExtra(EXTRA_NEW_PASSWORD);
                    handleActionChangePassword(accessToken, login, currentPassword, newPassword);
                    break;
                case ACTION_GENRES:
                    handleActionGenres();
                    break;
                case ACTION_CAN_CONTROL:
                    accessToken = intent.getStringExtra(EXTRA_TOKEN);
                    handleActionCanControl(accessToken);
                    break;
            }
        }
    }

    private void handleActionCanControl(String accessToken) {
        Call<CanControlResponse> request = mApi.canControl(accessToken);
        try {
            Response<CanControlResponse> response = request.execute();
            if (response.code() == HttpURLConnection.HTTP_OK) {
                CanControlResponse result = response.body();
                sendCanControlMessage(result);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendCanControlMessage(CanControlResponse result) {
        boolean canControl;
        String nickname;
        String avatar;
        if (result == null) {
            canControl = false;
            nickname = "Anonymous_" + UUID.randomUUID().toString();
            avatar = null;
        } else {
            canControl = result.isCanControl();
            nickname = result.getNickname();
            avatar = result.getAvatar();
        }
        Intent intent = new Intent(MovieViewV2Activity.FILTER);
        intent.putExtra(MovieViewV2Activity.EXTRA_CAN_CONTROL, canControl);
        intent.putExtra(MovieViewV2Activity.EXTRA_NICKNAME, nickname);
        intent.putExtra(MovieViewV2Activity.EXTRA_AVATAR, avatar);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }

    private void handleActionGenres() {
        Call<GenresResponse> request = mApi.genres();
        try {
            Response<GenresResponse> response = request.execute();
            if (response.code() == HttpURLConnection.HTTP_OK) {
                GenresResponse result = response.body();
                sendGenresMessage(result);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendGenresMessage(GenresResponse result) {
        if (result != null) {
            List<GenresResponse.Genre> genres = result.getGenres();
            if (genres != null) {
                Intent intent = new Intent(MoviesActivity.FILTER);
                intent.putExtra(MoviesActivity.EXTRA_GENRES, (ArrayList<GenresResponse.Genre>) genres);
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
            }
        }
    }

    private void handleActionChangePassword(String accessToken, String login, String currentPassword, String newPassword) {
        Call<ResponseBody> request = mApi.changePassword(accessToken, login, currentPassword, newPassword);
        try {
            Response<ResponseBody> response = request.execute();
            if (response.code() == HttpURLConnection.HTTP_OK) {
                sendChangePasswordMessage(response.body().string());
            } else {
                sendChangePasswordMessage(response.errorBody().string());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void sendChangePasswordMessage(String message) {
        Intent intent = new Intent(HomeActivity.FILTER);
        intent.putExtra(HomeActivity.EXTRA_CHANGE_PASSWORD_MESSAGE, message);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }

    private void handleActionGetUser(String accessToken) {
        Call<User> request = mApi.getUser(accessToken);
        try {
            Response<User> response = request.execute();
            if (response.code() == HttpURLConnection.HTTP_OK) {
                User user = response.body();
                sendGetUserSuccess(user);
            } else {
                String error = response.errorBody().string();
                sendGetUserFailure(error);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendGetUserFailure(String error) {
        Intent intent = new Intent(MoviesActivity.FILTER);
        intent.putExtra(MoviesActivity.EXTRA_ERROR, error);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }

    private void sendGetUserSuccess(User user) {
        Intent intent = new Intent(MoviesActivity.FILTER);
        intent.putExtra(MoviesActivity.EXTRA_USER, user);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }

    private void handleActionSignUp(String login, String password, String email, String avatar, boolean disseminator) {
        if (avatar != null && !"".equals(avatar)) {
            File file = new File(avatar);
            if (file.exists()) {
                byte[] bytes = convertFileToBytes(file);
                try {
                    boolean uploaded = Helper.uploadAvatar(getApplicationContext(), bytes, file.getName());
                    if (uploaded) {
                        completeSignUp(login, password, email, "files/avatars/" + file.getName(), disseminator);
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
        } else {
            completeSignUp(login, password, email, "", disseminator);
        }
    }

    private void completeSignUp(String login, String password, String email, String avatar, boolean disseminator){
        Call<ResponseBody> signUpRequest = mApi.signUp(login, password, email, avatar, disseminator);
        Response<ResponseBody> signUpResponse = null;
        try {
            signUpResponse = signUpRequest.execute();
            if (signUpResponse.code() == HttpURLConnection.HTTP_CREATED) {
                String signUpResult = signUpResponse.body().string();
                sendSignUpMessage(SignUpFragment.SUCCESS_MESSAGE, signUpResult);
            } else {
                String signUpResult = signUpResponse.errorBody().string();
                sendSignUpMessage(SignUpFragment.ERROR_MESSAGE, signUpResult);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void handleActionSignIn(String login, String password) {
        Call<Token> request = mApi.signIn(login, password);
        try {
            Response<Token> response = request.execute();
            if (response.code() == HttpURLConnection.HTTP_OK) {
                Token token = response.body();
                AppPreferences.Auth.setToken(getApplicationContext(), token);
                sendAuthSuccess();
            } else {
                sendAuthError(response.errorBody().string());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendAuthError(String message) {
        String resultForSending;
        try {
            JSONObject jsonObject = new JSONObject(message);
            resultForSending = jsonObject.getString("message");
        } catch (JSONException e) {
            resultForSending = message;
        }
        Intent intent = new Intent(AuthFragment.AUTH_FILTER);
        intent.putExtra(AuthFragment.ERROR_MESSAGE, resultForSending);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }

    private void sendSignUpMessage(String type, String message) {
        String resultForSending;
        try {
            JSONObject jsonObject = new JSONObject(message);
            resultForSending = jsonObject.getString("message");
        } catch (JSONException e) {
            resultForSending = message;
        }
        Intent intent = new Intent(SignUpFragment.FILTER);
        intent.putExtra(type, resultForSending);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }

    private void sendAuthSuccess() {
        Intent intent = new Intent(AuthFragment.AUTH_FILTER);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }

    private void handleActionUploadFile(String path, Movie movie, String posterPath) {
        File file = new File(path);
        if (!file.exists()) return;
        String fileName = file.getName();
        byte[] bytes = convertFileToBytes(file);
        if (bytes == null) return;
        try {
            boolean uploaded = Helper.uploadFile(getApplicationContext(), bytes, "video/mp4", fileName);
            if (uploaded) {
                File posterFile = new File(posterPath);
                byte[] posterBytes = convertFileToBytes(posterFile);
                boolean posterUploaded = Helper.uploadFile(getApplicationContext(), posterBytes, "image/*", posterFile.getName());
                if (posterUploaded) movie.setPosterUrl("/files/posters/" + posterFile.getName());
                else movie.setPosterUrl("");
                movie.setUrl("files/" + fileName);
                createMovie(movie);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    private void createMovie(Movie movie) {
        Call<ResponseBody> request = mApi.createMovie(movie);
        try {
            Response<ResponseBody> response = request.execute();
            if (response.code() == HttpURLConnection.HTTP_CREATED) {
                sendCreateMovieResponse(response.body().string());
            } else {
                sendCreateMovieResponse(response.errorBody().string());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendCreateMovieResponse(String message) {
        String messageForSending;
        try {
            JSONObject jsonObject = new JSONObject(message);
            messageForSending = jsonObject.getString("message");
        } catch (JSONException e) {
            messageForSending = message;
        }
        Intent intent = new Intent(HomeActivity.FILTER);
        intent.putExtra(HomeActivity.EXTRA_MOVIE_CREATED_MESSAGE, messageForSending);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }

    private byte[] convertFileToBytes(File file) {
        int size = (int) file.length();
        byte[] bytes = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
            buf.read(bytes, 0, bytes.length);
            buf.close();
            return bytes;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void handleActionCreateRoom(Room room) {
        Call<Room> request = mApi.createRoom(room);
        try {
            Response<Room> response = request.execute();
            Room result = response.body();
            sendMessage(result);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Gson gson = new GsonBuilder().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        mApi = retrofit.create(MovieApiService.class);
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionGetMovies(String genre) {
        Call<List<Movie>> request = mApi.getMovies(genre);
        try {
            Response<List<Movie>> response = request.execute();
            List<Movie> movies = response.body();
            sendMessge(movies);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendMessge(List<Movie> movies) {
        Intent intent = new Intent(MovieListFragment.GET_MOVIES_FILTER);
        intent.putExtra(MovieListFragment.EXTRA_MOVIES, (Serializable) movies);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }

    private void sendMessage(String message) {
        try {
            boolean exists = false;
            JSONObject jsonObject = new JSONObject(message);
            if (jsonObject.has(JSON_ROOM_EXISTS_KEY)) exists = jsonObject.getBoolean(JSON_ROOM_EXISTS_KEY);
            Intent intent = new Intent(MovieDetailActivity.ROOMS_FILTER);
            intent.putExtra(EXTRA_EXISTS, exists);
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void sendMessage(Room room) {
        Intent intent = new Intent(MovieDetailActivity.ROOMS_FILTER);
        intent.putExtra(MovieDetailActivity.EXTRA_ROOM, room);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }

    /**
     * Handle action Baz in the provided background thread with the provided
     * parameters.
     */
    private void handleActionRoomExists(int movieId, String roomName) {
        Call<ResponseBody> request = mApi.roomExists(movieId, roomName);
        try {
            Response<ResponseBody> response = request.execute();
            ResponseBody body = response.body();
            sendMessage(body.string());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
