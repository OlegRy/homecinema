package kpfu.itis.ru.homecinema.api;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import kpfu.itis.ru.homecinema.utils.AppPreferences;

public class Helper {

    private static final String TAG = Helper.class.getSimpleName();

    public static boolean uploadFile(Context context, byte[] bytes, String contentType, String fileName) throws MalformedURLException {
        URL requestUrl;
        String end = "\r\n";
        String twoHyphens = "--";
        String boundary = "++++**********+++++++++***************";
        Uri uri = null;

        Uri.Builder uriBuilder = null;
        uriBuilder = Uri.parse("http://172.20.10.5:4000").buildUpon();
        uri = uriBuilder.appendEncodedPath("upload")
                .build();
        requestUrl = new URL(uri.toString());
        try {
            HttpURLConnection conn = (HttpURLConnection) requestUrl.openConnection();
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setRequestMethod("POST");

            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
            conn.setRequestProperty("Authorization", AppPreferences.Auth.getAccessToken(context));
            DataOutputStream dos = new DataOutputStream(conn.getOutputStream());

            File photoFile = new File(context.getCacheDir(), fileName);
            photoFile.createNewFile();

            byte[] uploadData = bytes;
            FileOutputStream fos = new FileOutputStream(photoFile);
            fos.write(uploadData);
            fos.flush();
            fos.close();

            String uploadFile = photoFile.getName();

            FileInputStream fis = new FileInputStream(photoFile);
            int bufferSize = 1024;
            byte[] buffer = new byte[bufferSize];
            int length = -1;

            dos.writeBytes(twoHyphens + boundary);
            dos.writeBytes(end);
            dos.writeBytes("Content-Disposition: form-data; name=\"uploadedFile\";filename=\"" + uploadFile + "\"" + end);
            dos.writeBytes("Content-Type: " + contentType + end + end);

            while ((length = fis.read(buffer)) != -1) {
                dos.write(buffer, 0, length);
            }

            dos.writeBytes(end + twoHyphens + boundary + twoHyphens + end);
            fis.close();
            dos.flush();
            dos.close();

            if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
                Log.d(TAG, "error message: " + conn.getResponseMessage());
                return false;
            }

            StringBuffer sb  = new StringBuffer();
            InputStream is = conn.getInputStream();
            byte[] data = new byte[bufferSize];
            int leng = -1;
            while ((leng = is.read(data)) != -1) {
                sb.append(new String(data, 0, leng));
            }
            String result = sb.toString();
            JSONObject jsonObject = new JSONObject(result);
            Log.d(TAG, "result: " + result);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean uploadAvatar(Context context, byte[] bytes, String fileName) throws MalformedURLException {
        URL requestUrl;
        String end = "\r\n";
        String twoHyphens = "--";
        String boundary = "++++**********+++++++++***************";
        Uri uri = null;

        Uri.Builder uriBuilder = null;
        uriBuilder = Uri.parse("http://172.20.10.5:4000").buildUpon();
        uri = uriBuilder.appendEncodedPath("uploadAvatar")
                .build();
        requestUrl = new URL(uri.toString());
        try {
            HttpURLConnection conn = (HttpURLConnection) requestUrl.openConnection();
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setRequestMethod("PUT");

            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
            DataOutputStream dos = new DataOutputStream(conn.getOutputStream());

            File photoFile = new File(context.getCacheDir(), fileName);
            photoFile.createNewFile();

            byte[] uploadData = bytes;
            FileOutputStream fos = new FileOutputStream(photoFile);
            fos.write(uploadData);
            fos.flush();
            fos.close();

            String uploadFile = photoFile.getName();

            FileInputStream fis = new FileInputStream(photoFile);
            int bufferSize = 1024;
            byte[] buffer = new byte[bufferSize];
            int length = -1;

            dos.writeBytes(twoHyphens + boundary);
            dos.writeBytes(end);
            dos.writeBytes("Content-Disposition: form-data; name=\"uploadedFile\";filename=\"" + uploadFile + "\"" + end);
            dos.writeBytes("Content-Type: image/*" + end + end);

            while ((length = fis.read(buffer)) != -1) {
                dos.write(buffer, 0, length);
            }

            dos.writeBytes(end + twoHyphens + boundary + twoHyphens + end);
            fis.close();
            dos.flush();
            dos.close();

            if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
                Log.d(TAG, "error message: " + conn.getResponseMessage());
                return false;
            }

            StringBuffer sb  = new StringBuffer();
            InputStream is = conn.getInputStream();
            byte[] data = new byte[bufferSize];
            int leng = -1;
            while ((leng = is.read(data)) != -1) {
                sb.append(new String(data, 0, leng));
            }
            String result = sb.toString();
            JSONObject jsonObject = new JSONObject(result);
            Log.d(TAG, "result: " + result);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

}
