package kpfu.itis.ru.homecinema.utils;

import java.util.ArrayList;
import java.util.List;

import kpfu.itis.ru.homecinema.models.Movie;

public class DataUtil {

    public static List<Movie> getFakeMovies() {
        return new ArrayList<Movie>() {
            {
                add(new Movie("Interstellar", "cool movie", 100, 150, "20.01.2014", "https://upload.wikimedia.org/wikipedia/ru/c/ca/Interstellar_soundtrack_album_cover.jpg",
                        true));
                add(new Movie("Inception", "Leo DiCaprio rules!!", 100, 150, "20.01.2010", "https://upload.wikimedia.org/wikipedia/ru/7/76/%D0%9E%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0_%D1%81%D0%B0%D1%83%D0%BD%D0%B4%D1%82%D1%80%D0%B5%D0%BA%D0%B0_%D1%84%D0%B8%D0%BB%D1%8C%D0%BC%D0%B0_%D0%9D%D0%B0%D1%87%D0%B0%D0%BB%D0%BE.jpg",
                        true));
                add(new Movie("The Lion King", "Sad cartoon", 100, 150, "20.05.2001", "http://st.kp.yandex.net/images/album/cover_face/2800_200x200.jpg",
                        true));
                add(new Movie("Interstellar", "cool movie", 100, 150, "20.01.2014", "https://upload.wikimedia.org/wikipedia/ru/c/ca/Interstellar_soundtrack_album_cover.jpg",
                        true));
                add(new Movie("Inception", "Leo DiCaprio rules!!", 100, 150, "20.01.2010", "https://upload.wikimedia.org/wikipedia/ru/7/76/%D0%9E%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0_%D1%81%D0%B0%D1%83%D0%BD%D0%B4%D1%82%D1%80%D0%B5%D0%BA%D0%B0_%D1%84%D0%B8%D0%BB%D1%8C%D0%BC%D0%B0_%D0%9D%D0%B0%D1%87%D0%B0%D0%BB%D0%BE.jpg",
                        true));
                add(new Movie("The Lion King", "Sad cartoon", 100, 150, "20.05.2001", "http://st.kp.yandex.net/images/album/cover_face/2800_200x200.jpg",
                        true));
                add(new Movie("Interstellar", "cool movie", 100, 150, "20.01.2014", "https://upload.wikimedia.org/wikipedia/ru/c/ca/Interstellar_soundtrack_album_cover.jpg",
                        true));
                add(new Movie("Inception", "Leo DiCaprio rules!!", 100, 150, "20.01.2010", "https://upload.wikimedia.org/wikipedia/ru/7/76/%D0%9E%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0_%D1%81%D0%B0%D1%83%D0%BD%D0%B4%D1%82%D1%80%D0%B5%D0%BA%D0%B0_%D1%84%D0%B8%D0%BB%D1%8C%D0%BC%D0%B0_%D0%9D%D0%B0%D1%87%D0%B0%D0%BB%D0%BE.jpg",
                        true));
                add(new Movie("The Lion King", "Sad cartoon", 100, 150, "20.05.2001", "http://st.kp.yandex.net/images/album/cover_face/2800_200x200.jpg",
                        true));
                add(new Movie("Interstellar", "cool movie", 100, 150, "20.01.2014", "https://upload.wikimedia.org/wikipedia/ru/c/ca/Interstellar_soundtrack_album_cover.jpg",
                        true));
                add(new Movie("Inception", "Leo DiCaprio rules!!", 100, 150, "20.01.2010", "https://upload.wikimedia.org/wikipedia/ru/7/76/%D0%9E%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0_%D1%81%D0%B0%D1%83%D0%BD%D0%B4%D1%82%D1%80%D0%B5%D0%BA%D0%B0_%D1%84%D0%B8%D0%BB%D1%8C%D0%BC%D0%B0_%D0%9D%D0%B0%D1%87%D0%B0%D0%BB%D0%BE.jpg",
                        true));
                add(new Movie("The Lion King", "Sad cartoon", 100, 150, "20.05.2001", "http://st.kp.yandex.net/images/album/cover_face/2800_200x200.jpg",
                        true));
                add(new Movie("Interstellar", "cool movie", 100, 150, "20.01.2014", "https://upload.wikimedia.org/wikipedia/ru/c/ca/Interstellar_soundtrack_album_cover.jpg",
                        true));
                add(new Movie("Inception", "Leo DiCaprio rules!!", 100, 150, "20.01.2010", "https://upload.wikimedia.org/wikipedia/ru/7/76/%D0%9E%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0_%D1%81%D0%B0%D1%83%D0%BD%D0%B4%D1%82%D1%80%D0%B5%D0%BA%D0%B0_%D1%84%D0%B8%D0%BB%D1%8C%D0%BC%D0%B0_%D0%9D%D0%B0%D1%87%D0%B0%D0%BB%D0%BE.jpg",
                        true));
                add(new Movie("The Lion King", "Sad cartoon", 100, 150, "20.05.2001", "http://st.kp.yandex.net/images/album/cover_face/2800_200x200.jpg",
                        true));
            }
        };
    }

    public static List<String> getFakeUrls() {
        return new ArrayList<String>() {
            {
                add("https://upload.wikimedia.org/wikipedia/ru/c/ca/Interstellar_soundtrack_album_cover.jpg");
                add("https://upload.wikimedia.org/wikipedia/ru/7/76/%D0%9E%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0_%D1%81%D0%B0%D1%83%D0%BD%D0%B4%D1%82%D1%80%D0%B5%D0%BA%D0%B0_%D1%84%D0%B8%D0%BB%D1%8C%D0%BC%D0%B0_%D0%9D%D0%B0%D1%87%D0%B0%D0%BB%D0%BE.jpg");
                add("http://st.kp.yandex.net/images/album/cover_face/2800_200x200.jpg");
                add("https://upload.wikimedia.org/wikipedia/ru/c/ca/Interstellar_soundtrack_album_cover.jpg");
                add("https://upload.wikimedia.org/wikipedia/ru/7/76/%D0%9E%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0_%D1%81%D0%B0%D1%83%D0%BD%D0%B4%D1%82%D1%80%D0%B5%D0%BA%D0%B0_%D1%84%D0%B8%D0%BB%D1%8C%D0%BC%D0%B0_%D0%9D%D0%B0%D1%87%D0%B0%D0%BB%D0%BE.jpg");
                add("http://st.kp.yandex.net/images/album/cover_face/2800_200x200.jpg");
                add("https://upload.wikimedia.org/wikipedia/ru/c/ca/Interstellar_soundtrack_album_cover.jpg");
                add("https://upload.wikimedia.org/wikipedia/ru/7/76/%D0%9E%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0_%D1%81%D0%B0%D1%83%D0%BD%D0%B4%D1%82%D1%80%D0%B5%D0%BA%D0%B0_%D1%84%D0%B8%D0%BB%D1%8C%D0%BC%D0%B0_%D0%9D%D0%B0%D1%87%D0%B0%D0%BB%D0%BE.jpg");
                add("http://st.kp.yandex.net/images/album/cover_face/2800_200x200.jpg");
                add("https://upload.wikimedia.org/wikipedia/ru/c/ca/Interstellar_soundtrack_album_cover.jpg");
                add("https://upload.wikimedia.org/wikipedia/ru/7/76/%D0%9E%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0_%D1%81%D0%B0%D1%83%D0%BD%D0%B4%D1%82%D1%80%D0%B5%D0%BA%D0%B0_%D1%84%D0%B8%D0%BB%D1%8C%D0%BC%D0%B0_%D0%9D%D0%B0%D1%87%D0%B0%D0%BB%D0%BE.jpg");
                add("http://st.kp.yandex.net/images/album/cover_face/2800_200x200.jpg");
            }
        };
    }
}
