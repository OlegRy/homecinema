package kpfu.itis.ru.homecinema.ui.fragments;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kpfu.itis.ru.homecinema.R;
import kpfu.itis.ru.homecinema.services.MoviesService;
import kpfu.itis.ru.homecinema.ui.MoviesActivity;
import kpfu.itis.ru.homecinema.utils.UIUtil;

/**
 * A simple {@link Fragment} subclass.
 */
public class AuthFragment extends Fragment {

    public static final String ERROR_MESSAGE = "kpfu.itis.ru.homecinema.fragments.AuthFrament.ERROR";
    public static final String AUTH_FILTER = "kpfu.itis.ru.homecinema.fragments.AuthFrament.FILTER";

    private static final String BUNDLE_LOGIN = "kpfu.itis.ru.homecinema.fragments.AuthFrament.bundle.LOGIN";
    private static final String BUNDLE_PASSWORD = "kpfu.itis.ru.homecinema.fragments.AuthFrament.bundle.PASSWORD";

    private FragmentViewHolder mFragmentViewHolder;
    private MaterialDialog mProgressDialog;
    private String mLogin;
    private String mPassword;

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (mProgressDialog != null) mProgressDialog.dismiss();
            if (intent.hasExtra(ERROR_MESSAGE)) showError(intent.getStringExtra(ERROR_MESSAGE));
            else startApp();

        }
    };

    class FragmentViewHolder {
        @Bind(R.id.et_login) EditText mLoginEditText;
        @Bind(R.id.et_password) EditText mPasswordEditText;
        @Bind(R.id.tv_forgot_password) TextView mForgotPasswordTextView;
        @Bind(R.id.tv_sign_up) TextView mSignUpTextView;
        @Bind(R.id.btn_auth) Button mAuthButton;
        @Bind(R.id.tv_error_message) TextView mErrorMessageTextView;

        @OnClick(R.id.btn_auth)
        void onSignInButtonClick() {
            signIn();
        }

        @OnClick(R.id.tv_forgot_password)
        void onForgotPasswordButtonClick() {
            openForgotPasswordDialog();
        }

        @OnClick(R.id.tv_sign_up)
        void onSignUpButtonClick() {
            openSignUpScreen();
        }
    }

    class DialogViewHolder {
         @Bind(R.id.et_login_or_email) EditText mLoginOrPasswordEditText;
    }

    public AuthFragment() {
        // Required empty public constructor
    }

    public static AuthFragment newInstance(String login, String password) {
        AuthFragment fragment = new AuthFragment();
        Bundle args = new Bundle();
        args.putString(BUNDLE_LOGIN, login);
        args.putString(BUNDLE_PASSWORD, password);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((AppCompatActivity) getContext()).getSupportActionBar().hide();

        Bundle args = getArguments();
        if (args != null) {
            mLogin = args.getString(BUNDLE_LOGIN);
            mPassword = args.getString(BUNDLE_PASSWORD);
        }

        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mReceiver, new IntentFilter(AUTH_FILTER));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_auth, container, false);
        mFragmentViewHolder = new FragmentViewHolder();
        ButterKnife.bind(mFragmentViewHolder, view);
        if (mLogin != null && mPassword != null) {
            mFragmentViewHolder.mLoginEditText.setText(mLogin);
            mFragmentViewHolder.mPasswordEditText.setText(mPassword);
        }
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mReceiver);
    }

    private void openSignUpScreen() {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, new SignUpFragment())
                .addToBackStack(null)
                .commit();
    }

    private void openForgotPasswordDialog() {
        final DialogViewHolder viewHolder = new DialogViewHolder();
        MaterialDialog.Builder builder = new MaterialDialog.Builder(getContext());
        MaterialDialog dialog = builder.title(R.string.forgot_password_dialog_title)
                .customView(R.layout.dialog_reset_password, true)
                .positiveText(R.string.forgot_password_dialog_positive)
                .negativeText(R.string.forgot_password_dialog_negative)
                .autoDismiss(false)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        if (viewHolder.mLoginOrPasswordEditText != null) {
                            if (TextUtils.isEmpty(viewHolder.mLoginOrPasswordEditText.getText())) {
                                Toast.makeText(getContext(), R.string.empty_login_field_error, Toast.LENGTH_LONG).show();
                            } else {
                                resetPassword();
                                dialog.dismiss();
                            }
                        }
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .build();
        View view = dialog.getCustomView();
        ButterKnife.bind(viewHolder, view);
        dialog.show();
    }

    private void resetPassword() {
        // TODO: resetting password
    }

    private void signIn() {
        if (TextUtils.isEmpty(mFragmentViewHolder.mLoginEditText.getText()) || TextUtils.isEmpty(mFragmentViewHolder.mPasswordEditText.getText())) {
            showError(R.string.empty_fields_error);
            return;
        }
        mProgressDialog = UIUtil.showProgressDialog(getContext());
        MoviesService.startActionSignIn(getContext(), mFragmentViewHolder.mLoginEditText.getText().toString(), mFragmentViewHolder.mPasswordEditText.getText().toString());
    }

    private void startApp() {
        Intent intent = new Intent(getContext(), MoviesActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    private void showError(@StringRes int error) {
        mFragmentViewHolder.mErrorMessageTextView.setVisibility(View.VISIBLE);
        mFragmentViewHolder.mErrorMessageTextView.setText(error);
    }

    private void showError(String error) {
        mFragmentViewHolder.mErrorMessageTextView.setVisibility(View.VISIBLE);
        mFragmentViewHolder.mErrorMessageTextView.setText(error);
    }

}
