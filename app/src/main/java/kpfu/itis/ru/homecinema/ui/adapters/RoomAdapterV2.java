package kpfu.itis.ru.homecinema.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import kpfu.itis.ru.homecinema.R;
import kpfu.itis.ru.homecinema.models.Room;

public class RoomAdapterV2 extends RecyclerView.Adapter<RoomAdapterV2.ViewHolder> {

    private List<Room> mRooms;
    private OnRoomItemClickListener mListener;

    public RoomAdapterV2(List<Room> rooms, OnRoomItemClickListener listener) {
        mRooms = rooms;
        mListener = listener;
    }

    public interface OnRoomItemClickListener {
        void onRoomItemClick(Room room);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.room_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mNameTextView.setText(mRooms.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return mRooms.size();
    }

    public void addRoom(Room room) {
        mRooms.add(room);
        notifyItemInserted(mRooms.size() - 1);
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @Bind(R.id.tv_name) TextView mNameTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mListener != null) mListener.onRoomItemClick(mRooms.get(getAdapterPosition()));
        }
    }
}
