package kpfu.itis.ru.homecinema.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import kpfu.itis.ru.homecinema.R;
import kpfu.itis.ru.homecinema.models.GenresResponse;
import kpfu.itis.ru.homecinema.models.Movie;
import kpfu.itis.ru.homecinema.models.User;
import kpfu.itis.ru.homecinema.services.MoviesService;
import kpfu.itis.ru.homecinema.ui.fragments.MovieListFragment;
import kpfu.itis.ru.homecinema.utils.AppPreferences;
import kpfu.itis.ru.homecinema.utils.UIUtil;

public class MoviesActivity extends AppCompatActivity implements MovieListFragment.OnMovieClickListener {

    public static final String EXTRA_MOVIE = "movie";
    public static final String EXTRA_GENRES = "kpfu.itis.ru.homecinema.ui.MoviesActivity.extra.GENRES";

    private MaterialDialog mProgressDialog;
    private ViewHolder mViewHolder;
    private AppPagerAdapter mPagerAdapter;

    public static final String FILTER = "kpfu.itis.ru.homecinema.ui.MoviesActivity.FILTER";
    public static final String EXTRA_USER = "kpfu.itis.ru.homecinema.ui.MoviesActivity.extra.USER";
    public static final String EXTRA_ERROR = "kpfu.itis.ru.homecinema.ui.MoviesActivity.extra.ERROR";

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (mProgressDialog != null && mProgressDialog.isShowing()) mProgressDialog.dismiss();
            if (intent.hasExtra(EXTRA_USER)) {
                User user = (User) intent.getSerializableExtra(EXTRA_USER);
                startHomeActivity(user);
            } else if (intent.hasExtra(EXTRA_GENRES)) {
                List<GenresResponse.Genre> genres = (List<GenresResponse.Genre>) intent.getSerializableExtra(EXTRA_GENRES);
                populateTabs(genres);
            } else {
                showErrorDialog(intent.getStringExtra(EXTRA_ERROR));
            }
        }
    };

    private void populateTabs(List<GenresResponse.Genre> genres) {
        if (genres.size() == 0) {
            mViewHolder.mTabLayout.setVisibility(View.GONE);
            return;
        }
        mPagerAdapter = new AppPagerAdapter(getSupportFragmentManager(), genres);
        mViewHolder.mPager.setAdapter(mPagerAdapter);
        mViewHolder.mTabLayout.setupWithViewPager(mViewHolder.mPager);
    }

    class ViewHolder {

        @Bind(R.id.toolbar) Toolbar mToolbar;
        @Bind(R.id.pager) ViewPager mPager;
        @Bind(R.id.tab_layout) TabLayout mTabLayout;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movies);

        mViewHolder = new ViewHolder();
        ButterKnife.bind(mViewHolder, this);
        setSupportActionBar(mViewHolder.mToolbar);
        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, new IntentFilter(FILTER));
        mProgressDialog = UIUtil.showProgressDialog(this);
        MoviesService.startActionGenres(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_movies, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.me_home:
                goHome();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
    }

    private void showErrorDialog(String error) {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this);
        builder.title(R.string.error_title);
        builder.content(error);
        builder.positiveText(R.string.retry);
        builder.negativeText(R.string.cancel);
        builder.onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                mProgressDialog = UIUtil.showProgressDialog(MoviesActivity.this);
                MoviesService.startActionGetUser(MoviesActivity.this, AppPreferences.Auth.getAccessToken(MoviesActivity.this));
            }
        });
        builder.onNegative(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void startHomeActivity(User user) {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.putExtra(EXTRA_USER, user);
        startActivity(intent);
    }

    private void goHome() {
        String accessToken = AppPreferences.Auth.getAccessToken(this);
        if (accessToken == null || "".equals(accessToken)) {
            Intent intent = new Intent(this, HomeCinemaActivity.class);
            startActivity(intent);
        }
        else MoviesService.startActionGetUser(this, accessToken);
    }

    class AppPagerAdapter extends FragmentPagerAdapter {

        private static final int TABS_COUNT = 2;

        private List<GenresResponse.Genre> mGenres;

        public AppPagerAdapter(FragmentManager fm, List<GenresResponse.Genre> genres) {
            super(fm);
            mGenres = genres;
        }

        @Override
        public Fragment getItem(int position) {
            return MovieListFragment.newInstance(mGenres.get(position).getGenre());
        }

        @Override
        public int getCount() {
            return mGenres == null ? 0 : mGenres.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mGenres.get(position).getGenre();
        }
    }

    @Override
    public void onMovieClick(Movie movie) {
        openMovieDetail(movie);
    }

    private void openMovieDetail(Movie movie) {
        Intent intent = new Intent(this, MovieDetailActivity.class);
        intent.putExtra(EXTRA_MOVIE, movie);
        startActivity(intent);
    }
}
