package kpfu.itis.ru.homecinema.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GenresResponse {

    public static class Genre {

        @SerializedName("genre")
        @Expose
        private String mGenre;

        public String getGenre() {
            return mGenre;
        }

        public void setGenre(String genre) {
            mGenre = genre;
        }
    }

    @SerializedName("genres")
    @Expose
    private List<Genre> mGenres;

    public List<Genre> getGenres() {
        return mGenres;
    }

    public void setGenres(List<Genre> genres) {
        mGenres = genres;
    }
}
