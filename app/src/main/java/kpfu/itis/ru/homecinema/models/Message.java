package kpfu.itis.ru.homecinema.models;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Message {

    @SerializedName("author")
    private String mAuthor;

    @SerializedName("text")
    private String mText;

    @SerializedName("avatar")
    private String mAvatar;

    @SerializedName("date")
    private Date mDate;

    public String getAuthor() {
        return mAuthor;
    }

    public void setAuthor(String author) {
        mAuthor = author;
    }

    public String getText() {
        return mText;
    }

    public void setText(String text) {
        mText = text;
    }

    public String getAvatar() {
        return mAvatar;
    }

    public void setAvatar(String avatar) {
        mAvatar = avatar;
    }

    public Date getDate() {
        return mDate;
    }

    public void setDate(Date date) {
        mDate = date;
    }
}
