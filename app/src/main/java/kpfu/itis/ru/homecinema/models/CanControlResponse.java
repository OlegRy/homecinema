package kpfu.itis.ru.homecinema.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CanControlResponse {

    @SerializedName("message")
    @Expose
    private String mMessage;

    @SerializedName("canControl")
    @Expose
    private boolean mCanControl;

    @SerializedName("nickname")
    @Expose
    private String mNickname;

    @SerializedName("avatar")
    @Expose
    private String mAvatar;

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public boolean isCanControl() {
        return mCanControl;
    }

    public void setCanControl(boolean canControl) {
        mCanControl = canControl;
    }

    public String getNickname() {
        return mNickname;
    }

    public void setNickname(String nickname) {
        mNickname = nickname;
    }

    public String getAvatar() {
        return mAvatar;
    }

    public void setAvatar(String avatar) {
        mAvatar = avatar;
    }
}
