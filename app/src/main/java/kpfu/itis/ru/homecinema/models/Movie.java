package kpfu.itis.ru.homecinema.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Movie implements Parcelable {

    @SerializedName("id")
    private int mId;

    @SerializedName("title")
    private String mTitle;

    @SerializedName("description")
    private String mDescription;

    @SerializedName("duration")
    private int mDuration;

    @SerializedName("genre")
    private String mGenre;

    @SerializedName("posterUrl")
    private String mPosterUrl;

    @SerializedName("url")
    private String mUrl;

    @SerializedName("Rooms")
    private List<Room> mRooms;

    public Movie() {
    }

    public Movie(String title, String description, double price, int duration, String premiereDate, String posterUrl, boolean showing) {
        mTitle = title;
        mDescription = description;
        mDuration = duration;
        mPosterUrl = posterUrl;
    }

    public Movie(String title, String description, double price, int duration, String premiereDate, String posterUrl, boolean showing, String url) {
        mTitle = title;
        mDescription = description;
        mDuration = duration;
        mPosterUrl = posterUrl;
        mUrl = url;
    }

    protected Movie(Parcel in) {
        mId = in.readInt();
        mTitle = in.readString();
        mDescription = in.readString();
        mDuration = in.readInt();
        mGenre = in.readString();
        mPosterUrl = in.readString();
        mUrl = in.readString();
        int size = in.readInt();
        mRooms = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            Room room = (Room) in.readSerializable();
            mRooms.add(room);
        }
    }

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public int getDuration() {
        return mDuration;
    }

    public void setDuration(int duration) {
        mDuration = duration;
    }

    public String getGenre() {
        return mGenre;
    }

    public void setGenre(String genre) {
        mGenre = genre;
    }

    public String getPosterUrl() {
        return mPosterUrl;
    }

    public void setPosterUrl(String posterUrl) {
        mPosterUrl = posterUrl;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public List<Room> getRooms() {
        return mRooms;
    }

    public void setRooms(List<Room> rooms) {
        mRooms = rooms;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mId);
        dest.writeString(mTitle);
        dest.writeString(mDescription);
        dest.writeInt(mDuration);
        dest.writeString(mGenre);
        dest.writeString(mPosterUrl);
        dest.writeString(mUrl);
        dest.writeInt(mRooms == null ? 0 : mRooms.size());
        if (mRooms != null) {
            for (Room room : mRooms) {
                dest.writeSerializable(room);
            }
        }
    }
}
