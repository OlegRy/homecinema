package kpfu.itis.ru.homecinema.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Room implements Serializable {

    @SerializedName("room_id")
    private String mId;

    @SerializedName("name")
    private String mName;

    @SerializedName("currentTime")
    private int mCurrentTime;

    @SerializedName("MovieId")
    private int mMovieId;

    public Room() {}

    public Room(String id, String name, int movieId) {
        mId = id;
        mName = name;
        mCurrentTime = 0;
        mMovieId = movieId;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public int getCurrentTime() {
        return mCurrentTime;
    }

    public void setCurrentTime(int currentTime) {
        mCurrentTime = currentTime;
    }

    public int getMovieId() {
        return mMovieId;
    }

    public void setMovieId(int movieId) {
        mMovieId = movieId;
    }
}

