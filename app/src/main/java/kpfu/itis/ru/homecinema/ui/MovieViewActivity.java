package kpfu.itis.ru.homecinema.ui;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.VideoView;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import kpfu.itis.ru.homecinema.R;
import kpfu.itis.ru.homecinema.models.Room;

public class MovieViewActivity extends AppCompatActivity {

    private ViewHolder mViewHolder;
    private Socket mSocket;
    private Room mRoom;
    private String mUrl;

    private boolean mBuffered = false;

    private static final String URL = "http://172.20.10.5:4000/";
    private static final String SOCKET_TAG = "SOCKET";

    public static final String ACTION_GET_TIME = "getTime";

    private static final String EVENT_GET_TIME = ACTION_GET_TIME;
    private static final String EVENT_SET_TIME = "setTime";
    private static final String EVENT_ROOM = "room";
    private static final String EVENT_JOIN = "join";


    private void startPlaying(String url, int currentTime) {
        if (!mBuffered) {
            Log.d(SOCKET_TAG, "!mBuffered");
            String playingUrl = URL + mUrl;

            mViewHolder.mVideo.setVideoURI(Uri.parse(playingUrl));
            mViewHolder.mVideo.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
                        @Override
                        public void onBufferingUpdate(MediaPlayer mp, int percent) {
//                            if (mViewHolder.mVideo.isPlaying()) mViewHolder.mVideo.pause();
//                            Log.d(SOCKET_TAG, "percent is: " + percent);
                            if (percent > 3) {
                                if (!mBuffered) {
                                    Log.d(SOCKET_TAG, "percent is: " + percent);
                                    mBuffered = true;

                                    mSocket.emit(EVENT_GET_TIME, mRoom.getId());
                                }
                            }
                        }
                    });
                }
            });
            mViewHolder.mVideo.seekTo(currentTime * 1000);
//            mViewHolder.mVideo.start();
            //mViewHolder.mVideo.pause();
        } else {
            Log.d(SOCKET_TAG, "mBuffered");
            if (currentTime != 0) {
                mViewHolder.mVideo.seekTo(currentTime * 1000);
//                mViewHolder.mVideo.resume();
            }
            mViewHolder.mVideo.start();
//            mViewHolder.mVideo.start();
        }

//        mViewHolder.mVideo.start();
    }

    class ViewHolder {
        @Bind(R.id.video) VideoView mVideo;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_view);
        mViewHolder = new ViewHolder();
        ButterKnife.bind(mViewHolder, this);
        Intent intent = getIntent();
        if (intent != null) {
            mRoom = (Room) intent.getSerializableExtra(MovieDetailActivity.EXTRA_ROOM);
            mUrl = intent.getStringExtra(MovieDetailActivity.EXTRA_URL);
        }
//        startPlaying(URL, 0);
        createConnection();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mSocket.disconnect();
    }

    public void createConnection() {
        try {
            mSocket = IO.socket(URL);
            mSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {

                }
            }).on(Socket.EVENT_MESSAGE, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    if (args != null && args.length > 0) {
                        final JSONObject json = (JSONObject) args[0];
                        try {
                            if (json.has("url")) {
                                Log.i(SOCKET_TAG, "url: " + json.getString("url") + "; time: " + json.getInt("time"));
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            startPlaying(URL + mUrl, json.getInt("time"));
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {

                }
            }).on(EVENT_GET_TIME, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    int currentTime = mViewHolder.mVideo.getCurrentPosition() / 1000;
                    Log.d("tag", currentTime + " is current time; current position is: " + mViewHolder.mVideo.getCurrentPosition());
                    mSocket.emit(EVENT_SET_TIME, currentTime);
                }
            }).on(EVENT_ROOM, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    mSocket.emit(EVENT_JOIN, mRoom.getId());
                }
            });
            mSocket.connect();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSocket.disconnect();
    }
}
