package kpfu.itis.ru.homecinema.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kpfu.itis.ru.homecinema.R;
import kpfu.itis.ru.homecinema.models.Movie;
import kpfu.itis.ru.homecinema.models.Room;
import kpfu.itis.ru.homecinema.services.MoviesService;
import kpfu.itis.ru.homecinema.ui.adapters.RoomAdapterV2;
import kpfu.itis.ru.homecinema.ui.adapters.ScreenshotAdapter;
import kpfu.itis.ru.homecinema.utils.AppPreferences;
import kpfu.itis.ru.homecinema.utils.DataUtil;

public class MovieDetailActivity extends AppCompatActivity implements RoomAdapterV2.OnRoomItemClickListener {

    public static final String ROOMS_FILTER = "MovieDetailActivity.filter.ROOMS";

    public static final String EXTRA_MOVIE_ID = "MovieDetailActivity.extra.MOVIE_ID";
    public static final String EXTRA_ROOM_NAME = "MovieDetailActivity.extra.ROOM_NAME";
    public static final String EXTRA_ROOM = "MovieDetailActivity.extra.ROOM";
    public static final String EXTRA_URL = "MovieDetailActivity.extra.URL";

    private ActivityViewHolder mActivityViewHolder;
    private DialogViewHolder mDialogViewHolder;
    private Movie mMovie;
    private String mRoomName;
    private MaterialDialog mRoomDialog;
    private RoomAdapterV2 mRoomAdapter;
    private MaterialDialog mRoomCreationDialog;

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.hasExtra(MoviesService.EXTRA_EXISTS)) {
                boolean exists = intent.getBooleanExtra(MoviesService.EXTRA_EXISTS, false);
                if (exists) showErrorMessage(R.string.room_exists_error);
                else MoviesService.startActionCreateRoom(MovieDetailActivity.this, new Room("noid", mRoomName, mMovie.getId()));
            } else if (intent.hasExtra(EXTRA_ROOM)) {
                Room room = (Room) intent.getSerializableExtra(EXTRA_ROOM);
                mRoomAdapter.addRoom(room);
                startWatching(room);
            }

        }
    };

    private void startWatching(Room room) {
        Intent intent = new Intent(this, MovieViewV2Activity.class);
        intent.putExtra(EXTRA_ROOM, room);
        intent.putExtra(EXTRA_URL, mMovie.getUrl());
        startActivity(intent);
    }

    private void showErrorMessage(@StringRes int error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
        showRoomCreationDialog();
    }

    @Override
    public void onRoomItemClick(Room room) {
        startWatching(room);
    }

    class ActivityViewHolder {
        @Bind(R.id.toolbar) Toolbar mToolbar;
        @Bind(R.id.iv_avatar) ImageView mPosterImageView;
        @Bind(R.id.tv_description) TextView mDescriptionTextView;
        @Bind(R.id.rv_screenshots) RecyclerView mScreenshotsRecyclerView;
        @Bind(R.id.btn_watch) ImageButton mWatchButton;

        @OnClick(R.id.btn_watch)
        void onWatchButtonClick() {
            tryToWatch();
        }
    }

    class DialogViewHolder {
        @Bind(R.id.rv_rooms) RecyclerView mRoomsRecyclerView;
    }

    private void tryToWatch() {
        showRoomDialog();
    }

    private void showRoomDialog() {
        populateRoomDialog();
        mRoomDialog.show();
    }

    private void populateRoomDialog() {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this);
        builder.title(R.string.room_chooser_title);
        builder.customView(R.layout.dialog_room, false);
        if (AppPreferences.Auth.getAccessToken(this) != null) {
            builder.positiveText(R.string.room_creation_dialog_positive);
            builder.onPositive(new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    showRoomCreationDialog();
                }
            });
        }
        mRoomDialog = builder.build();
        View view = mRoomDialog.getCustomView();
        mDialogViewHolder = new DialogViewHolder();
        ButterKnife.bind(mDialogViewHolder, view);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        mRoomAdapter = new RoomAdapterV2(mMovie.getRooms(), this);
        mDialogViewHolder.mRoomsRecyclerView.setLayoutManager(manager);
        mDialogViewHolder.mRoomsRecyclerView.setAdapter(mRoomAdapter);
    }

    private void showRoomCreationDialog() {
        populateRoomCreationDialog();
        mRoomCreationDialog.show();
    }

    private void populateRoomCreationDialog() {
        mRoomCreationDialog = new MaterialDialog.Builder(this)
                .title(R.string.room_creation_dialog_title)
                .inputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME | InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS)
                .positiveText(R.string.room_creation_dialog_positive)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        checkRoom();
                    }
                })
                .input(R.string.room_creation_dialog_input_hint, R.string.room_creation_dialog_input_sample, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog dialog, CharSequence input) {

                    }
                })
                .build();
    }

    private void checkRoom() {
        mRoomName = mRoomCreationDialog.getInputEditText().getText().toString();
        MoviesService.startActionRoomExists(this, mMovie.getId(), mRoomName);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_deatail);
        mMovie = getCurrentMovie();
        mActivityViewHolder = new ActivityViewHolder();
        ButterKnife.bind(mActivityViewHolder, this);
        LinearLayoutManager manager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        ScreenshotAdapter adapter = new ScreenshotAdapter(this, DataUtil.getFakeUrls());
        mActivityViewHolder.mScreenshotsRecyclerView.setLayoutManager(manager);
        mActivityViewHolder.mScreenshotsRecyclerView.setAdapter(adapter);
        setSupportActionBar(mActivityViewHolder.mToolbar);
        initData();
        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, new IntentFilter(ROOMS_FILTER));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
    }

    private void initData() {
        if (mMovie != null) {
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) actionBar.setTitle(mMovie.getTitle());
            mActivityViewHolder.mDescriptionTextView.setText(mMovie.getDescription());
            Glide.with(this)
                    .load(getString(R.string.base_url) + mMovie.getPosterUrl())
                    .asBitmap()
                    .skipMemoryCache(false)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(mActivityViewHolder.mPosterImageView);
        }
    }

    private Movie getCurrentMovie() {
        Intent intent = getIntent();
        if (intent != null && intent.hasExtra(MoviesActivity.EXTRA_MOVIE)) return intent.getParcelableExtra(MoviesActivity.EXTRA_MOVIE);
        return null;
    }
}
