package kpfu.itis.ru.homecinema.ui.custom;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.Formatter;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.socket.client.Socket;
import kpfu.itis.ru.homecinema.R;
import kpfu.itis.ru.homecinema.ui.MovieViewV2Activity;

public class MediaContollerView extends FrameLayout {

    private static final String TAG = "MediaControllerView";

    private static final int sDefaultTimeout = 3000;
    private static final int FADE_OUT = 1;
    private static final int SHOW_PROGRESS = 2;

    private MediaPlayerControl mPlayer;
    private ControllerBot mBot;
    private Context mContext;
    private ViewGroup mAnchor;
    private View mRoot;
    private boolean mShowing;
    private boolean mDragging;
    private boolean mUseFastForward;
    private boolean mFromXml;
    private boolean mListenersSet;

    private OnClickListener mPauseListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            doPauseResume();
            show();
        }
    };

    private OnClickListener mFullscreenListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            doToggleFullscreen();
            show();
        }
    };

    private SeekBar.OnSeekBarChangeListener mSeekListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (mPlayer == null) return;
            if (!fromUser) return;
            if (!canControl()) return;
            if (mBot != null) mBot.onControlPressed();
            long duration = mPlayer.getDuration();
            long newPosition = (duration * progress) / 1000L;
            Socket socket = mWeakReference.get();
            if (socket != null) socket.emit(MovieViewV2Activity.EVENT_SEEK_TO, (int) newPosition);
            mPlayer.seekTo((int) newPosition);
            if (mCurrentTime != null) mCurrentTime.setText(stringForTime((int) newPosition));
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            show(3600000);
            mDragging = true;
            mHandler.removeMessages(SHOW_PROGRESS);
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            mDragging = false;
            setProgress();
            updatePausePlay();
            show();
            mHandler.sendEmptyMessage(SHOW_PROGRESS);
        }
    };

    private StringBuilder mFormatBuilder;
    private Formatter mFormatter;
    private WeakReference<Socket> mWeakReference;

    @Bind(R.id.btn_pause) ImageButton mPauseButton;
    @Bind(R.id.btn_fullscreen) ImageButton mFullscreenButton;
    @Bind(R.id.mediacontroller_progress) ProgressBar mProgress;
    @Bind(R.id.tv_time) TextView mTime;
    @Bind(R.id.tv_time_current) TextView mCurrentTime;

    private MessageHandler mHandler = new MessageHandler(this);

    public interface ControllerBot {

        void onControlPressed();
        boolean isTimeExpired();
    }

    public interface MediaPlayerControl {
        void start();
        void pause();
        int getDuration();
        int getCurrentPosition();
        void seekTo(int position);
        boolean isPlaying();
        int getBufferPercentage();
        boolean canPause();
        boolean canSeekBackward();
        boolean canSeekForward();
        boolean isFullScreen();
        void toggleFullscreen();
    }

    public MediaContollerView(Context context, Socket socket) {
        this(context, true, socket);
    }

    public MediaContollerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mRoot = null;
        mContext = context;
        mUseFastForward = true;
        mFromXml = true;
    }

    public MediaContollerView(Context context, boolean useFastForward, Socket socket) {
        super(context);
        mContext = context;
        mUseFastForward = useFastForward;
        mWeakReference = new WeakReference<>(socket);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (mRoot != null) initControllerView(mRoot);
    }

    public void setMediaPlayer(MediaPlayerControl player) {
        mPlayer = player;
        updatePausePlay();
        updateFullscreen();
    }

    public void setBot(ControllerBot bot) {
        mBot = bot;
    }

    public void setAnchorView(ViewGroup view) {
        mAnchor = view;
        ViewGroup.LayoutParams params = view.getLayoutParams();
//        FrameLayout.LayoutParams params = new LayoutParams(
//                ViewGroup.LayoutParams.MATCH_PARENT,
//                ViewGroup.LayoutParams.MATCH_PARENT
//        );
        removeAllViews();
        View v = makeControllerView();
        addView(v, params);
    }

    public void show() {
        show(sDefaultTimeout);
    }

    public void show(int timeout) {
        if (!mShowing && mAnchor != null) {
            setProgress();
            if (mPauseButton != null) mPauseButton.requestFocus();
            disableUnsupportedButtons();
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    Gravity.CENTER
            );
            mAnchor.addView(this, params);
            mShowing = true;
        }
        updatePausePlay();
        updateFullscreen();
        mHandler.sendEmptyMessage(SHOW_PROGRESS);

        Message msg = mHandler.obtainMessage(FADE_OUT);
        if (timeout != 0) {
            mHandler.removeMessages(FADE_OUT);
            mHandler.sendMessageDelayed(msg, timeout);
        }
    }

    public boolean isShowing() {
        return mShowing;
    }

    private View makeControllerView() {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mRoot = inflater.inflate(R.layout.media_controller, null);

        initControllerView(mRoot);
        return mRoot;
    }

    private void initControllerView(View v) {
        ButterKnife.bind(this, v);
        if (mPauseButton != null) {
            mPauseButton.requestFocus();
            mPauseButton.setOnClickListener(mPauseListener);
        }
        if (mFullscreenButton != null) {
            mFullscreenButton.requestFocus();
            mFullscreenButton.setOnClickListener(mFullscreenListener);
        }
        if (mProgress != null) {
            if (mProgress instanceof SeekBar) {
                SeekBar seekBar = (SeekBar) mProgress;
                seekBar.setOnSeekBarChangeListener(mSeekListener);
            }
            mProgress.setMax(1000);
        }
        mFormatBuilder = new StringBuilder();
        mFormatter = new Formatter(mFormatBuilder, Locale.getDefault());
    }

    private void updateFullscreen() {
        if (mRoot == null || mFullscreenButton == null || mPlayer == null) return;
        if (mPlayer.isFullScreen()) mFullscreenButton.setImageResource(R.drawable.ic_fullscreen_exit_white_36dp);
        else mFullscreenButton.setImageResource(R.drawable.ic_fullscreen_white_36dp);
    }

    private void updatePausePlay() {
        if (mRoot == null || mPauseButton == null || mPlayer == null) return;
        if (mPlayer.isPlaying()) mPauseButton.setImageResource(android.R.drawable.ic_media_pause);
        else mPauseButton.setImageResource(android.R.drawable.ic_media_play);
    }

    private void disableUnsupportedButtons() {
        if (mPlayer == null) return;
        try {
            if (mPauseButton != null && !mPlayer.canPause()) mPauseButton.setEnabled(false);
        } catch (IncompatibleClassChangeError e) {

        }

    }

    private static class MessageHandler extends Handler {

        private final WeakReference<MediaContollerView> mView;

        MessageHandler(MediaContollerView view) {
            mView = new WeakReference<MediaContollerView>(view);
        }

        @Override
        public void handleMessage(Message msg) {
            MediaContollerView view = mView.get();
            if (view == null || view.mPlayer == null) return;

            int pos;
            switch (msg.what) {
                case FADE_OUT:
                    view.hide();
                    break;
                case SHOW_PROGRESS:
                    pos = view.setProgress();
                    if (!view.mDragging && view.mShowing && view.mPlayer.isPlaying()) {
                        msg = obtainMessage(SHOW_PROGRESS);
                        sendMessageDelayed(msg, 1000 - (pos % 1000));
                    }
                    break;
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        show();
        return true;
    }

    @Override
    public boolean onTrackballEvent(MotionEvent event) {
        show();
        return false;
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (mPlayer == null) return true;
        int keyCode = event.getKeyCode();
        final boolean uniqueDown = event.getRepeatCount() == 0 && event.getAction() == KeyEvent.ACTION_DOWN;
        if (keyCode == KeyEvent.KEYCODE_HEADSETHOOK
                || keyCode == KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE
                || keyCode == KeyEvent.KEYCODE_SPACE) {
            if (uniqueDown) {
                doPauseResume();
                show();
                if (mPauseButton != null) mPauseButton.requestFocus();
            }
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_MEDIA_PLAY) {
            if (uniqueDown && !mPlayer.isPlaying()) {
                mPlayer.start();
                updatePausePlay();
                show();
            }
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN
                || keyCode == KeyEvent.KEYCODE_VOLUME_UP
                || keyCode == KeyEvent.KEYCODE_VOLUME_MUTE) {

            return super.dispatchKeyEvent(event);
        } else if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_MENU) {
            if (uniqueDown) hide();
            return true;
        }
        show();
        return super.dispatchKeyEvent(event);
    }

    @Override
    public void setEnabled(boolean enabled) {
        if (mPauseButton != null) {
            mPauseButton.setEnabled(enabled);
        }
        if (mProgress != null) {
            mProgress.setEnabled(enabled);
        }
        disableUnsupportedButtons();
        super.setEnabled(enabled);
    }

    private boolean canControl() {
        return mPlayer.canPause() && mPlayer.canSeekForward() && mPlayer.canSeekBackward();
    }

    private void doPauseResume() {
        if (mPlayer == null) return;
        if (!canControl()) return;
        if (mBot != null) mBot.onControlPressed();
        Socket socket = mWeakReference.get();
        if (socket != null) socket.emit(MovieViewV2Activity.EVENT_PLAY_PAUSE, mPlayer.isPlaying());
        if (mPlayer.isPlaying()) mPlayer.pause();
        else mPlayer.start();
        updatePausePlay();
    }

    private int setProgress() {
        if (mPauseButton == null || mDragging) return 0;
        int position = mPlayer.getCurrentPosition();
        int duration = mPlayer.getDuration();
        if (mProgress != null) {
            if (duration > 0) {
                long pos = 1000L * position / duration;
                mProgress.setProgress((int) pos);
            }
            int percent = mPlayer.getBufferPercentage();
            mProgress.setSecondaryProgress(percent * 10);
        }
        if (mTime != null) mTime.setText(stringForTime(duration));
        if (mCurrentTime != null) mCurrentTime.setText(stringForTime(position));
        return position;
    }

    public void hide() {
        if (mAnchor == null) return;
        try {
            mAnchor.removeView(this);
            mHandler.removeMessages(SHOW_PROGRESS);
        } catch (IllegalArgumentException e) {

        }
        mShowing = false;
    }

    private String stringForTime(int timeMs) {
        int totalSeconds = timeMs / 1000;
        int seconds = totalSeconds % 60;
        int minutes = (totalSeconds / 60) % 60;
        int hours = totalSeconds / 3600;

        mFormatBuilder.setLength(0);
        if (hours > 0) return mFormatter.format("%d:%02d:%02d", hours, minutes, seconds).toString();
        else return mFormatter.format("%02d:%02d", minutes, seconds).toString();

    }

    private void doToggleFullscreen() {
        if (mPlayer == null) return;
        mPlayer.toggleFullscreen();
    }


}
