package kpfu.itis.ru.homecinema.ui.adapters;

import android.content.Context;
import android.graphics.Point;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import kpfu.itis.ru.homecinema.R;
import kpfu.itis.ru.homecinema.models.Movie;

public class ShowingMovieAdapter extends RecyclerView.Adapter<ShowingMovieAdapter.ViewHolder> {

    private List<Movie> mMovies;
    private Context mContext;
    private Point mDisplaySize;
    private OnItemClickListener mCallback;

    public ShowingMovieAdapter(Context context, List<Movie> movies) {
        mMovies = movies;
        mContext = context;
        mDisplaySize = new Point();
        ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getSize(mDisplaySize);
    }

    public interface OnItemClickListener {
        void onItemClick(Movie movie);
    }

    public void setCallback(OnItemClickListener callback) {
        mCallback = callback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.showing_movie_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Glide.with(mContext)
                .load(mContext.getString(R.string.base_url) + mMovies.get(position).getPosterUrl())
                .asBitmap()
                .skipMemoryCache(false)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .placeholder(R.drawable.ic_launcher)
                .override(mDisplaySize.x / 2, mDisplaySize.x / 2)
                .into(holder.mPosterImageView);
        ViewGroup.LayoutParams layoutParams = holder.mInfoLinearLayout.getLayoutParams();
        layoutParams.width = mDisplaySize.x / 2;
        holder.mInfoLinearLayout.setLayoutParams(layoutParams);
        holder.mTitleTextView.setText(mMovies.get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        return mMovies.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @Bind(R.id.ll_info) LinearLayout mInfoLinearLayout;
        @Bind(R.id.iv_poster)ImageView mPosterImageView;
        @Bind(R.id.tv_title) TextView mTitleTextView;
        @Bind(R.id.tv_price) TextView mPriceTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mCallback != null) mCallback.onItemClick(mMovies.get(getAdapterPosition()));
        }
    }
}
