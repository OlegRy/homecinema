package kpfu.itis.ru.homecinema.utils;

import android.content.Context;
import android.content.SharedPreferences;

import kpfu.itis.ru.homecinema.models.Token;

public class AppPreferences {

    public static void clear(Context context) {
        Auth.getEditor(context).clear().commit();
    }

    public static class Auth {

        private static final String PREFERENCES_NAME = "kpfu.itis.ru.homecinema.preferences.AUTH";
        private static final String ACCESS_TOKEN = "kpfu.itis.ru.homecinema.preferences.auth.ACCESS_TOKEN";
        private static final String REFRESH_TOKEN = "kpfu.itis.ru.homecinema.preferences.auth.REFRESH_TOKEN";

        public static String getAccessToken(Context context) {
            return getPreferences(context).getString(ACCESS_TOKEN, null);
        }

        public static void setToken(Context context, Token token) {
            setAccessToken(context, token.getAccessToken());
            setRefreshToken(context, token.getRefreshToken());
        }

        public static void setAccessToken(Context context, String accessToken) {
            getEditor(context).putString(ACCESS_TOKEN, accessToken).commit();
        }

        public static String getRefreshToken(Context context) {
            return getPreferences(context).getString(REFRESH_TOKEN, null);
        }

        public static void setRefreshToken(Context context, String refreshToken) {
            getEditor(context).putString(REFRESH_TOKEN, refreshToken).commit();
        }

        private static SharedPreferences getPreferences(Context context) {
            return context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        }

        private static SharedPreferences.Editor getEditor(Context context) {
            return getPreferences(context).edit();
        }
    }
}
