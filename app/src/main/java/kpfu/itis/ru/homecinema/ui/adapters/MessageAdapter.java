package kpfu.itis.ru.homecinema.ui.adapters;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import kpfu.itis.ru.homecinema.R;
import kpfu.itis.ru.homecinema.models.Message;
import kpfu.itis.ru.homecinema.utils.DateUtil;

public class MessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Message> mMessages;
    private String mCurrentNickname;
    private Context mContext;

    private static final int CURRENT_USER_MESSAGE_TYPE = 0;
    private static final int OTHER_USERS_MESSAGE_TYPE = 1;

    public MessageAdapter(Context context, List<Message> messages, String currentNickname) {
        mMessages = messages;
        mCurrentNickname = currentNickname;
        mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case CURRENT_USER_MESSAGE_TYPE:
                return populateCurrentUserViewHolder(parent);
            case OTHER_USERS_MESSAGE_TYPE:
                return populateOtherUsersViewHolder(parent);
            default:
                return null;
        }
    }

    public void addMessage(Message message) {
        mMessages.add(0, message);
        notifyItemInserted(0);
    }

    private OtherUsersViewHolder populateOtherUsersViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_other_users_message, parent, false);
        return new OtherUsersViewHolder(view);
    }

    private CurrentUserViewHolder populateCurrentUserViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_current_user_message, parent, false);
        return new CurrentUserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int itemViewType = getItemViewType(position);
        if (itemViewType == CURRENT_USER_MESSAGE_TYPE) bindCurrentUserViewHolder(holder, position);
        else bindOtherUsersViewHolder(holder, position);
    }

    private void bindOtherUsersViewHolder(RecyclerView.ViewHolder holder, int position) {
        OtherUsersViewHolder viewHolder = (OtherUsersViewHolder) holder;
        if (mMessages.get(position).getAvatar() != null && !"".equals(mMessages.get(position).getAvatar())) {
            populateImageView(viewHolder.mAvatarImageView, mMessages.get(position).getAvatar());
        } else {
            populateImageView(viewHolder.mAvatarImageView);
        }
        viewHolder.mMessageTextView.setText(mMessages.get(position).getText());
        viewHolder.mDateTextView.setText(DateUtil.formatDate(mMessages.get(position).getDate()));
    }

    private void bindCurrentUserViewHolder(RecyclerView.ViewHolder holder, int position) {
        CurrentUserViewHolder viewHolder = (CurrentUserViewHolder) holder;
        if (mMessages.get(position).getAvatar() != null && !"".equals(mMessages.get(position).getAvatar())) {
            populateImageView(viewHolder.mAvatarImageView, mMessages.get(position).getAvatar());
        } else {
            populateImageView(viewHolder.mAvatarImageView);
        }
        viewHolder.mMessageTextView.setText(mMessages.get(position).getText());
        viewHolder.mDateTextView.setText(DateUtil.formatDate(mMessages.get(position).getDate()));
    }

    private void populateImageView(ImageView avatarImageView) {
        Drawable color = new ColorDrawable(ContextCompat.getColor(mContext, R.color.colorPrimary));
        Drawable image = ContextCompat.getDrawable(mContext, R.drawable.ic_person_white_36dp);
        LayerDrawable layerDrawable = new LayerDrawable(new Drawable[]{color, image});
        avatarImageView.setImageDrawable(layerDrawable);
    }

    private void populateImageView(ImageView avatarImageView, String path) {
        Glide.with(mContext)
                .load(mContext.getString(R.string.base_url) + "/" + path)
                .asBitmap()
                .centerCrop()
                .skipMemoryCache(false)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(avatarImageView);
    }

    @Override
    public int getItemCount() {
        return mMessages == null ? 0 : mMessages.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (mMessages.get(position).getAuthor().equals(mCurrentNickname)) return CURRENT_USER_MESSAGE_TYPE;
        return OTHER_USERS_MESSAGE_TYPE;
    }

    public class CurrentUserViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.iv_avatar) ImageView mAvatarImageView;
        @Bind(R.id.tv_message) TextView mMessageTextView;
        @Bind(R.id.tv_date) TextView mDateTextView;

        public CurrentUserViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public class OtherUsersViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.iv_avatar) ImageView mAvatarImageView;
        @Bind(R.id.tv_message) TextView mMessageTextView;
        @Bind(R.id.tv_date) TextView mDateTextView;

        public OtherUsersViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
